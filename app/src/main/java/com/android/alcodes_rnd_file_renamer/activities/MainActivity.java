package com.android.alcodes_rnd_file_renamer.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.adapters.TabAdapter;
import com.android.alcodes_rnd_file_renamer.fragments.MainFragment;
import com.android.alcodes_rnd_file_renamer.fragments.RenameTaskFragment;
import com.android.alcodes_rnd_file_renamer.fragments.RenameTaskFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements MainFragment.Callbacks{
    private TabAdapter mTabAdapter;

    protected TabLayout tabLayoutContent;
    protected ViewPager viewPagerNavBar;
    public static MainActivity currentMainActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        tabLayoutContent = findViewById(R.id.tablayout_nav_bar);
        viewPagerNavBar = findViewById(R.id.viewpager_content);

        mTabAdapter = new TabAdapter(getSupportFragmentManager());
        mTabAdapter.addFragment(new MainFragment(),getString(R.string.tab_add_file));
        mTabAdapter.addFragment(new RenameTaskFragment(), getString(R.string.tab_rename_task));

        viewPagerNavBar.setAdapter(mTabAdapter);
        tabLayoutContent.setupWithViewPager(viewPagerNavBar);
        currentMainActivity = this;
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        if(fragment instanceof MainFragment){
            MainFragment mainFragment = (MainFragment) fragment;
            mainFragment.setCallback(this);
        }
    }

    @Override
    public void onNextIconClicked() {
        viewPagerNavBar.setCurrentItem(1);
    }

    @Override
    public void onBackPressed() {
        if(viewPagerNavBar.getCurrentItem() == 0){
            finish();
        }else{
            viewPagerNavBar.setCurrentItem(0);
        }
    }
}
