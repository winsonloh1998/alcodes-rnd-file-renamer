package com.android.alcodes_rnd_file_renamer.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.adapters.TabRenameAdapter;
import com.android.alcodes_rnd_file_renamer.fragments.PreviewFragment;
import com.android.alcodes_rnd_file_renamer.fragments.RenameCriterionFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class RenameActivity extends AppCompatActivity implements RenameCriterionFragment.Callbacks{
    public static final String EXTRA_LONG_TASK_ID = "EXTRA_LONG_TASK_ID";

    private TabRenameAdapter mTabRenameAdapter;
    protected TabLayout tabLayoutContent;
    protected ViewPager viewPagerNavBar;

    private List<Object> selectedCriterionList = new ArrayList<>();
    private List<Long> criterionBridgeId = new ArrayList<>();
    private long taskIdForPreview = 0L;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rename);

        Intent extra = getIntent();
        long taskId = 0;

        if(extra != null){
            taskId = extra.getLongExtra(EXTRA_LONG_TASK_ID,0 );
        }

        tabLayoutContent = findViewById(R.id.tablayout_nav_bar);
        viewPagerNavBar = findViewById(R.id.viewpager_content);

        mTabRenameAdapter = new TabRenameAdapter(getSupportFragmentManager());
        mTabRenameAdapter.setTaskId(taskId);
        mTabRenameAdapter.addFragment(RenameCriterionFragment.newInstance(taskId),getString(R.string.tab_rename_criterion));
        mTabRenameAdapter.addFragment(new PreviewFragment(), getString(R.string.tab_preview_name));

        viewPagerNavBar.setAdapter(mTabRenameAdapter);
        tabLayoutContent.setupWithViewPager(viewPagerNavBar);
    }

    public List<Object> getSelectedCriterionList(){
        return selectedCriterionList;
    }

    public void setCriterionBridgeId(List<Long> id){
        this.criterionBridgeId = id;
    }

    public List<Long> getCriterionBridgeId(){
        return criterionBridgeId;
    }

    public void setTaskIdForPreview(long id){
        this.taskIdForPreview = id;
    }

    public long getTaskIdForPreview(){
        return taskIdForPreview;
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        if(fragment instanceof RenameCriterionFragment){
            RenameCriterionFragment renameCriterionFragment = (RenameCriterionFragment) fragment;
            renameCriterionFragment.setCallback(this);
        }
    }

    @Override
    public void onNextIconClicked(List<Object> selectedCriterionList) {
        this.selectedCriterionList = selectedCriterionList;
        viewPagerNavBar.setCurrentItem(1);
    }

    @Override
    public void onBackPressed() {
        if(viewPagerNavBar.getCurrentItem() == 0){
            finish();
        }else{
            viewPagerNavBar.setCurrentItem(0);
        }
    }
}
