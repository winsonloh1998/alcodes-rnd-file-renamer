package com.android.alcodes_rnd_file_renamer.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertCharacters;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.android.alcodes_rnd_file_renamer.database.entities.NewName;
import com.android.alcodes_rnd_file_renamer.database.entities.ReplaceCharacter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CriterionAdapter extends RecyclerView.Adapter<CriterionAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public CriterionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_created_criterions,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CriterionAdapter.ViewHolder holder, int position) {
        holder.bindTo(mData.get(position) ,mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data){
        if(data == null){
            mData = new ArrayList<>();
        }else{
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder implements Serializable {
        public Long id;
        public Date createdDate;
        public String nameOfCriterion;
        public Object typeOfCriterion;
        public boolean isChecked = false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public RelativeLayout root;
        public TextView mTextViewCriterionName;
        public TextView mTextViewCriterionDescription;
        public ImageView mImageViewTickIfSelected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            root = itemView.findViewById(R.id.relativelayout_root);
            mTextViewCriterionName = itemView.findViewById(R.id.textview_criterion_name);
            mTextViewCriterionDescription = itemView.findViewById(R.id.textview_criterion_description);
            mImageViewTickIfSelected = itemView.findViewById(R.id.imageview_tick_if_selected);
        }

        public void bindTo(DataHolder data, Callbacks callbacks){
            resetViews();

            if(data != null){

                if(data.isChecked){
                    mImageViewTickIfSelected.setVisibility(View.VISIBLE);
                }else{
                    mImageViewTickIfSelected.setVisibility(View.GONE);
                }

                if(data.typeOfCriterion instanceof ReplaceCharacter){

                    ReplaceCharacter criterion = (ReplaceCharacter) data.typeOfCriterion;
                    mTextViewCriterionName.setText(itemView.getContext().getString(R.string.add_replace_char_task));

                    String matchCases;
                    if(criterion.getMatchCases()){
                        matchCases = itemView.getContext().getString(R.string.description_sensitive);
                    }else{
                        matchCases = itemView.getContext().getString(R.string.description_insensitive);
                    }

                    mTextViewCriterionDescription.setText(
                            itemView.getContext().getString(R.string.description_replace_char,
                                    criterion.getCharactersToReplace(),
                                    criterion.getReplaceWith(),
                                    matchCases));

                }else if(data.typeOfCriterion instanceof NewName){

                    NewName criterion = (NewName) data.typeOfCriterion;
                    mTextViewCriterionName.setText(itemView.getContext().getString(R.string.add_new_name_task));
                    mTextViewCriterionDescription.setText(
                            itemView.getContext().getString(R.string.description_new_name, criterion.getNewName()));

                }else if(data.typeOfCriterion instanceof InsertCharacters){

                    InsertCharacters criterion = (InsertCharacters) data.typeOfCriterion;
                    mTextViewCriterionName.setText(itemView.getContext().getString(R.string.add_insert_char_task));

                    String insertAtPosition =
                            returnDescBasedOnInsertPosition(criterion.getInsertAtOption(),
                                    itemView, criterion.getBackward(), criterion.getPosition());

                    mTextViewCriterionDescription.setText(
                            itemView.getContext().getString(R.string.description_insert_char,
                                    criterion.getCharactersToInsert(), insertAtPosition));

                }else if(data.typeOfCriterion instanceof InsertNumber){
                    InsertNumber criterion = (InsertNumber) data.typeOfCriterion;
                    mTextViewCriterionName.setText(itemView.getContext().getString(R.string.add_insert_number_task));

                    String insertAtPosition =
                            returnDescBasedOnInsertPosition(criterion.getInsertAtOption(),
                                    itemView, criterion.getBackward(), criterion.getPosition());

                    String withOrWithoutAutoNumber;
                    if(criterion.getAutoNumber()){
                        switch (criterion.getIncrementDecrementOption()){
                            case 0:
                                withOrWithoutAutoNumber = itemView.getContext().getString(R.string.description_with_increment)
                                                            + criterion.getIncrementDecrementValue();
                                break;
                            case 1:
                                withOrWithoutAutoNumber = itemView.getContext().getString(R.string.description_with_decrement)
                                                            + criterion.getIncrementDecrementValue();
                                break;
                            default:
                                withOrWithoutAutoNumber = "";
                        }
                    }else{
                        withOrWithoutAutoNumber = "";
                    }

                    mTextViewCriterionDescription.setText(
                            itemView.getContext().getString(R.string.description_insert_number, criterion.getNumbersToInsert(),
                                    insertAtPosition, withOrWithoutAutoNumber));
                }else if(data.typeOfCriterion instanceof InsertFileSize){
                    InsertFileSize criterion = (InsertFileSize) data.typeOfCriterion;
                    mTextViewCriterionName.setText(itemView.getContext().getString(R.string.add_insert_file_size_task));

                    String showUnit;
                    if(criterion.getShowUnit()){
                        showUnit = itemView.getContext().getString(R.string.description_with_unit);
                    }else{
                        showUnit = String.valueOf((char)8);
                    }

                    String insertAtPosition =
                            returnDescBasedOnInsertPosition(criterion.getInsertAtOption(),
                                    itemView, criterion.getBackward(), criterion.getPosition());

                    mTextViewCriterionDescription.setText(
                            itemView.getContext().getString(R.string.description_insert_file_size,
                                    itemView.getContext().getResources().getStringArray(R.array.file_size_format_spinner)[criterion.getFormatSizeOption()],
                                    showUnit, criterion.getDecimalPlaces(),
                                    insertAtPosition));
                }
            }

            if(callbacks != null){
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbacks.onListItemClicked(data, mImageViewTickIfSelected);
                    }
                });
            }
        }

        public void resetViews(){
            mTextViewCriterionName.setText("");
            mTextViewCriterionDescription.setText("");
            root.setClickable(false);
        }

        private String returnDescBasedOnInsertPosition(int insertAtOption, View itemView, boolean isBackward, int position){
            String insertAtPosition;
            switch (insertAtOption){
                case 0:
                    insertAtPosition = itemView.getContext().getString(R.string.description_before_name);
                    break;
                case 1:
                    insertAtPosition = itemView.getContext().getString(R.string.description_after_name);
                    break;
                case 2:
                    if(isBackward){
                        insertAtPosition = itemView.getContext().getString(R.string.description_from_backward_ch)+
                                itemView.getContext().getString(R.string.description_at) + position +
                                itemView.getContext().getString(R.string.description_place) +
                                itemView.getContext().getString(R.string.description_from_backward_en);
                    }else{
                        insertAtPosition = itemView.getContext().getString(R.string.description_at) + position +
                                itemView.getContext().getString(R.string.description_place);
                    }
                    break;
                default:
                    insertAtPosition = "";
                    break;
            }
            return insertAtPosition;
        }
    }

    public List<DataHolder> getSelectedCriterion(){
        List<DataHolder> selectedCriterionList = new ArrayList<>();
        for(int i=0 ; i < mData.size(); i++){
            if(mData.get(i).isChecked){
                selectedCriterionList.add(mData.get(i));
            }
        }
        return selectedCriterionList;
    }

    public interface Callbacks{
        void onListItemClicked(DataHolder data, ImageView imageViewTick);
    }
}
