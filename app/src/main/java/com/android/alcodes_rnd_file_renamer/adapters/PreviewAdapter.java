package com.android.alcodes_rnd_file_renamer.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.database.entities.File;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();

    @NonNull
    @Override
    public PreviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview_renamed_file,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PreviewAdapter.ViewHolder holder, int position) {
        holder.bindTo(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data){
        if(data == null){
            mData = new ArrayList<>();
        }else{
            mData = data;
        }
    }

    public static class DataHolder{
        public String beforeRename;
        public String afterRename;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mTextViewBeforeRenamedFileName;
        public TextView mTextViewAfterRenamedFileName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextViewBeforeRenamedFileName = itemView.findViewById(R.id.textview_before_rename_file_name);
            mTextViewAfterRenamedFileName = itemView.findViewById(R.id.textview_after_rename_file_name);
        }

        public void bindTo(DataHolder data){
            resetView();

            if(data != null){
                mTextViewBeforeRenamedFileName.setText(data.beforeRename);
                mTextViewAfterRenamedFileName.setText(data.afterRename);
            }
        }

        public void resetView(){
            mTextViewBeforeRenamedFileName.setText("");
            mTextViewAfterRenamedFileName.setText("");
        }
    }
}
