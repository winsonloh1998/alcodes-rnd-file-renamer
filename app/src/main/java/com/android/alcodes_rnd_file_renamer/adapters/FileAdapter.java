package com.android.alcodes_rnd_file_renamer.adapters;

import android.graphics.Point;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.database.entities.File;
import com.bumptech.glide.Glide;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {
    private List<File> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_file, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<File> data){
        if(data == null){
            mData = new ArrayList<>();
        }else{
            mData = data;
        }
    }

    public void setCallBacks(Callbacks callbacks){mCallbacks = callbacks; }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        protected RelativeLayout mRelativeLayoutRoot;
        protected ImageView mImageViewFileType;
        protected TextView mTextViewFileName;
        protected TextView mTextViewFileType;
        protected TextView mTextViewFileSize;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            mRelativeLayoutRoot = itemView.findViewById(R.id.relativelayout_root);
            mImageViewFileType = itemView.findViewById(R.id.imageview_file_type);
            mTextViewFileName = itemView.findViewById(R.id.textview_file_name);
            mTextViewFileType = itemView.findViewById(R.id.textview_file_type);
            mTextViewFileSize = itemView.findViewById(R.id.textview_file_size);
        }

        public void bindTo(File data, Callbacks callbacks){
            resetView();

            if(data != null){
                if(data.isChecked()){
                    Uri drawableUri = Uri.parse("android.resource://com.android.alcodes_rnd_file_renamer/drawable/ic_check_blue_24dp");
                    Glide.with(itemView)
                            .load(drawableUri)
                            .into(mImageViewFileType);
                }else{
                    if(data.getFileType().toLowerCase().equals("png") ||
                            data.getFileType().toLowerCase().equals("jpg") ||
                            data.getFileType().toLowerCase().equals("gif") ||
                            data.getFileType().toLowerCase().equals("jpeg")){
                        Glide.with(itemView)
                                .load(data.getFileUri())
                                .into(mImageViewFileType);
                    }else{
                        Uri drawableUri = Uri.parse("android.resource://com.android.alcodes_rnd_file_renamer/drawable/ic_insert_drive_file_blue_24dp");
                        Glide.with(itemView)
                                .load(drawableUri)
                                .into(mImageViewFileType);
                    }
                }

                mTextViewFileName.setText(data.getFileName());
                mTextViewFileSize.setText(itemView.getContext().getString(R.string.text_view_size) + " " + data.getFileSize() + " bytes");
                mTextViewFileType.setText(itemView.getContext().getString(R.string.text_view_type)+ " " + data.getFileType());

                if(callbacks != null){
                    mRelativeLayoutRoot.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data, mImageViewFileType);
                        }
                    });
                }
            }

        }

        public void resetView(){
            mImageViewFileType.setImageResource(R.drawable.ic_insert_drive_file_blue_24dp);
            mTextViewFileName.setText("");
            mTextViewFileType.setText("Type: ");
            mTextViewFileSize.setText("Size: ");
        }
    }

    public List<File> getSelectedFile(){
        List<File> selectedFileList = new ArrayList<>();
        for(int i=0; i < mData.size(); i++){
            if(mData.get(i).isChecked()){
                selectedFileList.add(mData.get(i));
            }
        }
        return selectedFileList;
    }

    public interface Callbacks {
        void onListItemClicked(File data, View view);
    }
}
