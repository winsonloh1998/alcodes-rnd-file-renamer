package com.android.alcodes_rnd_file_renamer.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.android.alcodes_rnd_file_renamer.fragments.PreviewFragment;
import com.android.alcodes_rnd_file_renamer.fragments.RenameCriterionFragment;

import java.security.cert.PKIXRevocationChecker;
import java.util.ArrayList;
import java.util.List;

public class TabRenameAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private Long taskId;

    public TabRenameAdapter(FragmentManager fragmentManager) {super(fragmentManager);}

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return RenameCriterionFragment.newInstance(taskId);
            case 1:
                return new PreviewFragment();
        }
        return null;
    }

    public void addFragment(Fragment fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void setTaskId(Long taskId){
        this.taskId = taskId;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
