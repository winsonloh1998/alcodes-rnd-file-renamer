package com.android.alcodes_rnd_file_renamer.adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.alcodes_rnd_file_renamer.R;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.nio.channels.SeekableByteChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_created_tasks, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TaskAdapter.ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data){
        if(data == null){
            mData = new ArrayList<>();
        }else{
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder implements Serializable {
        public Long id;
        public String taskName;
        public Date taskCreatedDate;
        public boolean isChecked = false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public RelativeLayout root;
        public TextView mTextViewTaskName;
        public TextView mTextViewTaskCreatedDate;
        public ImageView mImageViewTick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            root = itemView.findViewById(R.id.relativelayout_root);
            mTextViewTaskName = itemView.findViewById(R.id.textview_task_name);
            mTextViewTaskCreatedDate = itemView.findViewById(R.id.textview_task_created_date);
            mImageViewTick = itemView.findViewById(R.id.imageview_tick_if_selected);
        }

        public void bindTo(DataHolder data, Callbacks callbacks){
            resetViews();

            if(data != null){
                if(data.isChecked){
                    mImageViewTick.setVisibility(View.VISIBLE);
                }else{
                    mImageViewTick.setVisibility(View.GONE);
                }

                mTextViewTaskName.setText(data.taskName);
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                mTextViewTaskCreatedDate.setText(itemView.getContext().getString(R.string.text_view_created_date)+ " " + dateFormat.format(data.taskCreatedDate));
            }

            if(callbacks != null){
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbacks.onListItemClicked(data, mImageViewTick);
                    }
                });
            }
        }

        public void resetViews() {
            mTextViewTaskName.setText("");
            mTextViewTaskCreatedDate.setText("");
            root.setClickable(false);
        }
    }

    public List<DataHolder> getSelectedTask(){
        List<DataHolder> selectedTaskList = new ArrayList<>();
        for(int i=0 ; i < mData.size(); i++){
            if(mData.get(i).isChecked){
                selectedTaskList.add(mData.get(i));
            }
        }
        return selectedTaskList;
    }

    public List<DataHolder> getAllTask(){
        return mData;
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder dataHolder, ImageView imageViewTick);
    }
}
