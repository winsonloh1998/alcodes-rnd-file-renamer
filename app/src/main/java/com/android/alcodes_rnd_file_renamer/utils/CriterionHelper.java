package com.android.alcodes_rnd_file_renamer.utils;

public class CriterionHelper {
    public String replaceCharacterCriterion(String fileName, String charToReplace, String replaceWith, boolean matchCases){
        String newName = "";
        newName = fileName;

        if(matchCases){
            newName = newName.replaceAll(charToReplace, replaceWith);
        }else{
            newName = newName.replaceAll("(?i)"+charToReplace,replaceWith);
        }
        return newName;
    }

    public String newNameCriterion(String replaceWithNewName, int numberOfExistedFileName){
        String newName = "";

        if(numberOfExistedFileName == 0){
            newName = replaceWithNewName;
        }else{
            newName = replaceWithNewName + "("+ numberOfExistedFileName + ")";
        }

        return newName;
    }

    public String insertCharactersCriterion(String fileName, String charToInsert, int insertAtOption, int position, boolean backward) {
        String newName = "";

        switch (insertAtOption) {
            case 0:
                newName = charToInsert + fileName;
                break;
            case 1:
                newName = fileName + charToInsert;
                break;
            case 2:

                if (backward) {
                    //Backward Counting
                    position = fileName.length() - position;

                    //Prevent Index Out Of Bound
                    if (position < 0) {
                        position = -1;
                    }

                    newName = fileName.substring(0, position + 1)
                            + charToInsert
                            + fileName.substring(position + 1);
                } else {
                    //Forward Counting

                    //Prevent Index Out Of Bound
                    if (position > fileName.length()) {
                        position = fileName.length() + 1;
                    }

                    newName = fileName.substring(0, position - 1)
                            + charToInsert
                            + fileName.substring(position - 1);
                }
                break;
        }

        return newName;
    }

    public String insertNumberCriterion(String fileName, String numberToInsert, boolean autoNumber, int incrementDecrementOption,
                                        int incrementDecrementValue, int insertAtOption, int position, boolean backward, int numberOfFile){
        String newName = "";
        int upDownValue = 0;
        int numberOfZeroOccur = 0;

        if(autoNumber){
            switch (incrementDecrementOption){
                case 0:
                    upDownValue = +(incrementDecrementValue) * numberOfFile;
                    break;
                case 1:
                    upDownValue = -(incrementDecrementValue) * numberOfFile;
                    break;
            }

            for(int i=0; i<numberToInsert.length(); i++){
                if(numberToInsert.charAt(i) != '0'){
                    break;
                }

                if(i == (numberToInsert.length() - 1)){
                    break;
                }

                numberOfZeroOccur++;
            }

            int finalValue = Integer.parseInt(numberToInsert) + upDownValue;

            if(finalValue < 0){
                numberToInsert = String.format("%0"+(numberOfZeroOccur+2)+"d", finalValue);
            }else{
                numberToInsert = String.format("%0"+(numberOfZeroOccur+1)+"d", finalValue);
            }

        }

        switch (insertAtOption) {
            case 0:
                newName = numberToInsert + fileName;
                break;
            case 1:
                newName = fileName + numberToInsert;
                break;
            case 2:
                if (backward) {
                    //Backward Counting
                    position = fileName.length() - position;

                    //Prevent Index Out Of Bound
                    if (position < 0) {
                        position = -1;
                    }

                    newName = fileName.substring(0, position + 1)
                            + numberToInsert
                            + fileName.substring(position + 1);
                } else {
                    //Forward Counting

                    //Prevent Index Out Of Bound
                    if (position > fileName.length()) {
                        position = fileName.length() + 1;
                    }

                    newName = fileName.substring(0, position - 1)
                            + numberToInsert
                            + fileName.substring(position - 1);
                }
                break;
        }

        return newName;
    }

    public String insertFileSize(String fileName, Double fileSize, int formatSizeOption, int decimalPlaces,
                                 boolean showUnit, int insertAtOption, int position, boolean backward){
        String newName = "";
        String format = "";
        String tempFormat = "";
        Double fileSizeInFormat = 0.0;

        final double KILOBYTES = 1024;
        final double MEGABYTES = KILOBYTES * KILOBYTES;
        final double GIGABYTES = KILOBYTES * MEGABYTES;

        switch (formatSizeOption){
            case 0:
                //Auto is Selected
                if(fileSize > KILOBYTES){
                    fileSizeInFormat = fileSize/KILOBYTES;
                    tempFormat = "KB ";
                }else if (fileSize > MEGABYTES){
                    fileSizeInFormat = fileSize/MEGABYTES;
                    tempFormat = "MB ";
                }else if (fileSize > GIGABYTES){
                    fileSizeInFormat = fileSize/GIGABYTES;
                    tempFormat = "GB ";
                }else{
                    fileSizeInFormat = fileSize;
                    tempFormat = "Bytes ";
                }
                break;
            case 1:
                //Bytes is Selected
                fileSizeInFormat = fileSize;
                tempFormat = "Bytes";

                break;
            case 2:
                //KB is Selected
                fileSizeInFormat = fileSize/KILOBYTES;
                tempFormat = "KB ";
                break;
            case 3:
                //MB is Selected
                fileSizeInFormat = fileSize/MEGABYTES;
                tempFormat = "MB ";
                break;
            case 4:
                //GB is Selected
                fileSizeInFormat = fileSize/GIGABYTES;
                tempFormat = "GB ";
                break;
        }

        if(showUnit){
            format = tempFormat;
        }else{
            format = "";
        }

        switch (insertAtOption) {
            case 0:
                newName = String.format("%.0"+decimalPlaces+"f", fileSizeInFormat) + format + fileName;
                break;
            case 1:
                newName = fileName + String.format("%.0"+decimalPlaces+"f", fileSizeInFormat) + format;
                break;
            case 2:
                if (backward) {
                    //Backward Counting
                    position = fileName.length() - position;

                    //Prevent Index Out Of Bound
                    if (position < 0) {
                        position = -1;
                    }

                    newName = fileName.substring(0, position + 1)
                            + String.format("%.0"+decimalPlaces+"f", fileSizeInFormat) + format
                            + fileName.substring(position + 1);
                } else {
                    //Forward Counting

                    //Prevent Index Out Of Bound
                    if (position > fileName.length()) {
                        position = fileName.length() + 1;
                    }

                    newName = fileName.substring(0, position - 1)
                            + String.format("%.0"+decimalPlaces+"f", fileSizeInFormat) + format
                            + fileName.substring(position - 1);
                }
                break;
        }

        return newName;
    }
}
