package com.android.alcodes_rnd_file_renamer.utils;

import android.content.Context;

import com.android.alcodes_rnd_file_renamer.database.DbOpenHelper;
import com.android.alcodes_rnd_file_renamer.database.entities.DaoMaster;
import com.android.alcodes_rnd_file_renamer.database.entities.DaoSession;

public class DatabaseHelper {

    private static DaoSession mInstance;

    public static DaoSession getInstance(Context context){
        if(mInstance == null){
            synchronized (DatabaseHelper.class){
                if(mInstance == null){
                    DbOpenHelper dbOpenHelper = new DbOpenHelper(context, "app");

                    mInstance = new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
                }
            }
        }
        return mInstance;
    }

    private DatabaseHelper(){
    }
}
