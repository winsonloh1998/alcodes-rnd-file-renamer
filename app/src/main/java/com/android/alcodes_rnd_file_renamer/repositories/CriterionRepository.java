package com.android.alcodes_rnd_file_renamer.repositories;

import android.content.Context;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.alcodes_rnd_file_renamer.adapters.CriterionAdapter;
import com.android.alcodes_rnd_file_renamer.database.entities.Criterion;
import com.android.alcodes_rnd_file_renamer.database.entities.CriterionDao;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertCharacters;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertCharactersDao;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertFileSizeDao;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertNumberDao;
import com.android.alcodes_rnd_file_renamer.database.entities.NewName;
import com.android.alcodes_rnd_file_renamer.database.entities.NewNameDao;
import com.android.alcodes_rnd_file_renamer.database.entities.ReplaceCharacter;
import com.android.alcodes_rnd_file_renamer.database.entities.ReplaceCharacterDao;
import com.android.alcodes_rnd_file_renamer.utils.DatabaseHelper;

import org.greenrobot.greendao.query.DeleteQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class CriterionRepository {
    private static CriterionRepository mInstance;

    private MutableLiveData<List<CriterionAdapter.DataHolder>> mCriterionAdapterListLiveData = new MutableLiveData<>();

    public static CriterionRepository getInstance(){
        if(mInstance == null){
            synchronized (CriterionRepository.class){
                mInstance = new CriterionRepository();
            }
        }

        return mInstance;
    }

    private CriterionRepository(){}

    public LiveData<List<CriterionAdapter.DataHolder>> getCriterionAdapterListLiveData(){
        return mCriterionAdapterListLiveData;
    }

    //Criterion Table
    public void loadCriterionAdapterList(Context context, Long taskId){
        List<CriterionAdapter.DataHolder> dataHolders = new ArrayList<>();

        List<Criterion> records = DatabaseHelper.getInstance(context)
                .getCriterionDao()
                .queryBuilder().where(CriterionDao.Properties.TaskId.eq(taskId)).list();

        if(records != null){
            for(Criterion criterion : records){
                CriterionAdapter.DataHolder dataHolder = new CriterionAdapter.DataHolder();
                dataHolder.id = criterion.getId();
                dataHolder.createdDate = criterion.getCreatedDate();

                if(criterion.getTypeOfCriterion().equals("RC")){
                    //Replace Characters
                    dataHolder.nameOfCriterion = "Replace Characters";
                    dataHolder.typeOfCriterion = getReplaceCharacter(context, criterion.getCriterionId());
                }else if(criterion.getTypeOfCriterion().equals("NN")){
                    //New Name
                    dataHolder.nameOfCriterion = "New Name";
                    dataHolder.typeOfCriterion = getNewName(context, criterion.getCriterionId());
                }else if(criterion.getTypeOfCriterion().equals("IC")){
                    //Insert Characters
                    dataHolder.nameOfCriterion = "Insert Characters";
                    dataHolder.typeOfCriterion = getInsertCharacters(context, criterion.getCriterionId());
                }else if(criterion.getTypeOfCriterion().equals("IN")){
                    //Insert Number
                    dataHolder.nameOfCriterion = "Insert Number";
                    dataHolder.typeOfCriterion = getInsertNumber(context, criterion.getCriterionId());
                }else if(criterion.getTypeOfCriterion().equals("IFS")){
                    //Insert File Size
                    dataHolder.nameOfCriterion = "Insert File Size";
                    dataHolder.typeOfCriterion = getInsertFileSize(context, criterion.getCriterionId());
                }

                dataHolder.isChecked = criterion.getIsChecked();

                dataHolders.add(dataHolder);
            }
        }

        mCriterionAdapterListLiveData.setValue(dataHolders);
    }

    private void addCriterion(Context context, Long taskId, Long criterionId, String typeOfCriterion){
        Criterion criterion = new Criterion();
        criterion.setTaskId(taskId);
        criterion.setCriterionId(criterionId);
        criterion.setCreatedDate(new Date());
        criterion.setTypeOfCriterion(typeOfCriterion);
        criterion.setIsChecked(false);

        DatabaseHelper.getInstance(context)
                .getCriterionDao()
                .save(criterion);
    }

    private void deleteCriterion(Context context, Long taskId, Long criterionId, String typeOfCriterion){
        DeleteQuery<Criterion> criterionDeleteQuery = DatabaseHelper.getInstance(context)
                .getCriterionDao()
                .queryBuilder()
                .where(CriterionDao.Properties.TaskId.eq(taskId),
                        CriterionDao.Properties.CriterionId.eq(criterionId),
                        CriterionDao.Properties.TypeOfCriterion.eq(typeOfCriterion))
                .buildDelete();

        criterionDeleteQuery.executeDeleteWithoutDetachingEntities();

        loadCriterionAdapterList(context, taskId);
    }

    public void editCriterion(Context context, Long id, Long taskId, boolean isChecked){
        CriterionDao criterionDao = DatabaseHelper.getInstance(context).getCriterionDao();
        Criterion criterion = criterionDao.load(id);

        if(criterion != null){
            criterion.setIsChecked(isChecked);
            criterionDao.update(criterion);
            loadCriterionAdapterList(context, taskId);
        }
    }

    //Replace Character Table
    public void addReplaceCharacter(Context context, String charToReplace,
                                    String replaceWith, boolean matchCases, Long taskId){
        ReplaceCharacter replaceCharacter = new ReplaceCharacter();
        replaceCharacter.setCharactersToReplace(charToReplace);
        replaceCharacter.setReplaceWith(replaceWith);
        replaceCharacter.setMatchCases(matchCases);

        DatabaseHelper.getInstance(context)
                .getReplaceCharacterDao()
                .save(replaceCharacter);

        //Save into Bridge Table
        addCriterion(context,taskId,replaceCharacter.getId(),"RC");

        loadCriterionAdapterList(context, taskId);
    }

    private ReplaceCharacter getReplaceCharacter(Context context, Long id){
        ReplaceCharacter replaceCharacter = DatabaseHelper.getInstance(context)
                .getReplaceCharacterDao().load(id);

        return replaceCharacter;
    }

    public void editReplaceCharacter(Context context, Long id, String charToReplace,
                                     String replaceWith, boolean matchCases, Long taskId){
        ReplaceCharacterDao replaceCharacterDao = DatabaseHelper.getInstance(context).getReplaceCharacterDao();
        ReplaceCharacter replaceCharacter = replaceCharacterDao.load(id);

        if(replaceCharacter != null){
            replaceCharacter.setCharactersToReplace(charToReplace);
            replaceCharacter.setReplaceWith(replaceWith);
            replaceCharacter.setMatchCases(matchCases);

            replaceCharacterDao.update(replaceCharacter);

            loadCriterionAdapterList(context, taskId);
        }
    }

    public void deleteReplaceCharacter(Context context, Long id, String typeOfCriterion, Long taskId){
        DatabaseHelper.getInstance(context)
                .getReplaceCharacterDao()
                .deleteByKey(id);

        deleteCriterion(context, taskId, id, typeOfCriterion);
    }



    //New Name Table
    public void addNewName(Context context, String replaceWithNewName, Long taskId){
        NewName newName = new NewName();
        newName.setNewName(replaceWithNewName);

        DatabaseHelper.getInstance(context)
                .getNewNameDao()
                .save(newName);

        //Save into Bridge Table
        addCriterion(context, taskId, newName.getId(),"NN");

        loadCriterionAdapterList(context, taskId);
    }

    private NewName getNewName(Context context, Long id){
        NewName newName = DatabaseHelper.getInstance(context)
                .getNewNameDao().load(id);

        return newName;
    }

    public void editNewName(Context context, Long id, String replaceWithNewName, Long taskId){
        NewNameDao newNameDao = DatabaseHelper.getInstance(context).getNewNameDao();
        NewName newName = newNameDao.load(id);

        if(newName != null){
            newName.setNewName(replaceWithNewName);

            newNameDao.update(newName);

            loadCriterionAdapterList(context, taskId);
        }
    }

    public void deleteNewName(Context context, Long id, String typeOfCriterion, Long taskId){
        DatabaseHelper.getInstance(context)
                .getNewNameDao()
                .deleteByKey(id);

        deleteCriterion(context, taskId, id, typeOfCriterion);
    }



    //Insert Character Table
    public void addInsertCharacters(Context context, String charToInsert,
                                    int insertAtOption, int position, boolean backward, Long taskId){
        InsertCharacters insertCharacters = new InsertCharacters();
        insertCharacters.setCharactersToInsert(charToInsert);
        insertCharacters.setInsertAtOption(insertAtOption);
        insertCharacters.setPosition(position);
        insertCharacters.setBackward(backward);

        DatabaseHelper.getInstance(context)
                .getInsertCharactersDao()
                .save(insertCharacters);

        //Save into Bridge Table
        addCriterion(context, taskId, insertCharacters.getId(), "IC");

        loadCriterionAdapterList(context, taskId);
    }

    private InsertCharacters getInsertCharacters(Context context, Long id){
        InsertCharacters insertCharacters = DatabaseHelper.getInstance(context)
                .getInsertCharactersDao().load(id);

        return insertCharacters;
    }

    public void editInsertCharacters(Context context, Long id, String charToInsert,
                                     int insertAtOption, int position, boolean backward, Long taskId){
        InsertCharactersDao insertCharactersDao = DatabaseHelper.getInstance(context).getInsertCharactersDao();
        InsertCharacters insertCharacters = insertCharactersDao.load(id);

        if(insertCharacters != null){
            insertCharacters.setCharactersToInsert(charToInsert);
            insertCharacters.setInsertAtOption(insertAtOption);
            insertCharacters.setPosition(position);
            insertCharacters.setBackward(backward);

            insertCharactersDao.update(insertCharacters);

            loadCriterionAdapterList(context, taskId);
        }
    }

    public void deleteInsertCharacters(Context context, Long id, String typeOfCriterion, Long taskId){
        DatabaseHelper.getInstance(context)
                .getInsertCharactersDao()
                .deleteByKey(id);

        deleteCriterion(context, taskId, id, typeOfCriterion);
    }



    //Insert Number Table
    public void addInsertNumber(Context context, String numberToInsert, boolean autoNumber,
                                int incrementDecrementOption, int incrementDecrementValue, int insertAtOption,
                                int position, boolean backward, Long taskId){
        InsertNumber insertNumber = new InsertNumber();
        insertNumber.setNumbersToInsert(numberToInsert);
        insertNumber.setAutoNumber(autoNumber);
        insertNumber.setIncrementDecrementOption(incrementDecrementOption);
        insertNumber.setIncrementDecrementValue(incrementDecrementValue);
        insertNumber.setInsertAtOption(insertAtOption);
        insertNumber.setPosition(position);
        insertNumber.setBackward(backward);

        DatabaseHelper.getInstance(context)
                .getInsertNumberDao()
                .save(insertNumber);

        //Save into Bridge Table
        addCriterion(context, taskId, insertNumber.getId(), "IN");

        loadCriterionAdapterList(context, taskId);
    }

    private InsertNumber getInsertNumber(Context context, Long id){
        InsertNumber insertNumber = DatabaseHelper.getInstance(context)
                .getInsertNumberDao().load(id);

        return insertNumber;
    }

    public void editInsertNumber(Context context, Long id, String numberToInsert, boolean autoNumber,
                                 int incrementDecrementOption, int incrementDecrementValue, int insertAtOption,
                                 int position, boolean backward, Long taskId){

        InsertNumberDao insertNumberDao = DatabaseHelper.getInstance(context).getInsertNumberDao();
        InsertNumber insertNumber = insertNumberDao.load(id);

        if(insertNumber != null){
            insertNumber.setNumbersToInsert(numberToInsert);
            insertNumber.setAutoNumber(autoNumber);
            insertNumber.setIncrementDecrementOption(incrementDecrementOption);
            insertNumber.setIncrementDecrementValue(incrementDecrementValue);
            insertNumber.setInsertAtOption(insertAtOption);
            insertNumber.setPosition(position);
            insertNumber.setBackward(backward);

            insertNumberDao.update(insertNumber);

            loadCriterionAdapterList(context, taskId);
        }
    }

    public void deleteInsertNumber(Context context, Long id, String typeOfCriterion, Long taskId){
        DatabaseHelper.getInstance(context)
                .getInsertNumberDao()
                .deleteByKey(id);

        deleteCriterion(context, taskId, id, typeOfCriterion);
    }



    //Insert File Size Table
    public void addInsertFileSize(Context context, int formatSizeOption, int decimalPlaces,
                                  boolean showUnit, int insertAtOption, int position, boolean backward, Long taskId){
        InsertFileSize insertFileSize = new InsertFileSize();
        insertFileSize.setFormatSizeOption(formatSizeOption);
        insertFileSize.setDecimalPlaces(decimalPlaces);
        insertFileSize.setShowUnit(showUnit);
        insertFileSize.setInsertAtOption(insertAtOption);
        insertFileSize.setPosition(position);
        insertFileSize.setBackward(backward);

        DatabaseHelper.getInstance(context)
                .getInsertFileSizeDao()
                .save(insertFileSize);

        //Save into Bridge Table
        addCriterion(context, taskId, insertFileSize.getId(), "IFS");

        loadCriterionAdapterList(context, taskId);
    }

    private InsertFileSize getInsertFileSize(Context context, Long id){
        InsertFileSize insertFileSize = DatabaseHelper.getInstance(context)
                .getInsertFileSizeDao().load(id);

        return  insertFileSize;
    }

    public void editInsertFileSize(Context context, Long id, int formatSizeOption, int decimalPlaces,
                                   boolean showUnit, int insertAtOption, int position, boolean backward, Long taskId){
        InsertFileSizeDao insertFileSizeDao = DatabaseHelper.getInstance(context).getInsertFileSizeDao();
        InsertFileSize insertFileSize = insertFileSizeDao.load(id);

        if(insertFileSize != null){
            insertFileSize.setFormatSizeOption(formatSizeOption);
            insertFileSize.setDecimalPlaces(decimalPlaces);
            insertFileSize.setShowUnit(showUnit);
            insertFileSize.setInsertAtOption(insertAtOption);
            insertFileSize.setPosition(position);
            insertFileSize.setBackward(backward);

            insertFileSizeDao.update(insertFileSize);

            loadCriterionAdapterList(context, taskId);
        }
    }

    public void deleteInsertFileSize(Context context, Long id, String typeOfCriterion, Long taskId){
        DatabaseHelper.getInstance(context)
                .getInsertFileSizeDao()
                .deleteByKey(id);

        deleteCriterion(context, taskId, id, typeOfCriterion);
    }
}
