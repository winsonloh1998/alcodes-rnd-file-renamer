package com.android.alcodes_rnd_file_renamer.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.alcodes_rnd_file_renamer.adapters.TaskAdapter;

import com.android.alcodes_rnd_file_renamer.database.entities.Criterion;
import com.android.alcodes_rnd_file_renamer.database.entities.CriterionDao;
import com.android.alcodes_rnd_file_renamer.database.entities.Task;
import com.android.alcodes_rnd_file_renamer.database.entities.TaskDao;
import com.android.alcodes_rnd_file_renamer.utils.DatabaseHelper;

import org.greenrobot.greendao.query.DeleteQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskRepository {
    private static TaskRepository mInstance;

    private MutableLiveData<List<TaskAdapter.DataHolder>> mTaskAdapterListLiveData = new MutableLiveData<>();

    public static TaskRepository getInstance(){
        if(mInstance == null){
            synchronized (TaskRepository.class){
                mInstance = new TaskRepository();
            }
        }

        return mInstance;
    }

    private TaskRepository() {
    }

    public LiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListLiveData(){
        return mTaskAdapterListLiveData;
    }

    public void loadTaskAdapterList(Context context){
        List<TaskAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<Task> records = DatabaseHelper.getInstance(context)
                .getTaskDao()
                .loadAll();

        if(records != null){
            for(Task task : records){
                TaskAdapter.DataHolder dataHolder = new TaskAdapter.DataHolder();
                dataHolder.id = task.getId();
                dataHolder.taskName = task.getTaskName();
                dataHolder.taskCreatedDate = task.getCreatedDate();
                dataHolder.isChecked = task.getIsChecked();
                dataHolders.add(dataHolder);
            }
        }

        mTaskAdapterListLiveData.setValue(dataHolders);
    }

    public void editIsChecked(Context context, Long id, boolean isChecked){
        TaskDao taskDao = DatabaseHelper.getInstance(context).getTaskDao();
        Task task = taskDao.load(id);

        if(task != null){
            task.setIsChecked(isChecked);
            taskDao.update(task);
            loadTaskAdapterList(context);
        }
    }

    public void addTask(Context context, String name){

        Task task = new Task();
        task.setTaskName(name);
        task.setCreatedDate(new Date());
        task.setIsChecked(false);

        DatabaseHelper.getInstance(context)
                .getTaskDao()
                .save(task);

        loadTaskAdapterList(context);
    }

    public void editTask(Context context, Long id, String name){
        TaskDao taskDao = DatabaseHelper.getInstance(context).getTaskDao();
        Task task = taskDao.load(id);

        if(task != null){
            task.setTaskName(name);

            taskDao.update(task);

            loadTaskAdapterList(context);
        }
    }

    public void deleteTask(Context context, Long id){
        deleteCorrespondingCriterion(context, id);

        DatabaseHelper.getInstance(context)
                .getTaskDao()
                .deleteByKey(id);
    }

    private void deleteCorrespondingCriterion(Context context, Long id){
        DeleteQuery<Criterion> criterionDeleteQuery = DatabaseHelper.getInstance(context)
                .getCriterionDao()
                .queryBuilder()
                .where(CriterionDao.Properties.TaskId.eq(id))
                .buildDelete();

        criterionDeleteQuery.executeDeleteWithoutDetachingEntities();
    }
}
