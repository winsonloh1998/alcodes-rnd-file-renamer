package com.android.alcodes_rnd_file_renamer.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.activities.RenameActivity;
import com.android.alcodes_rnd_file_renamer.adapters.CriterionAdapter;
import com.android.alcodes_rnd_file_renamer.database.entities.Criterion;
import com.android.alcodes_rnd_file_renamer.database.entities.File;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertCharacters;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.android.alcodes_rnd_file_renamer.database.entities.NewName;
import com.android.alcodes_rnd_file_renamer.database.entities.ReplaceCharacter;
import com.android.alcodes_rnd_file_renamer.fragments.RenameTaskFragment;
import com.android.alcodes_rnd_file_renamer.utils.SharedPreferenceHelper;
import com.android.alcodes_rnd_file_renamer.viewmodels.Criterion.CriterionViewModel;
import com.android.alcodes_rnd_file_renamer.viewmodels.Criterion.CriterionViewModelFactory;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RenameCriterionFragment extends Fragment implements CriterionAdapter.Callbacks {
    private static final String ARG_LONG_TASK_ID = "ARG_LONG_TASK_ID";

    private Animation fabOpen, fabClose, fabClock, fabAntiClock;

    protected TextView mTextViewNoRenameCriterionCreated;
    protected RecyclerView mRecycleViewCriterionCreated;

    protected TextView mTextViewReplaceChars;
    protected TextView mTextViewNewName;
    protected TextView mTextViewInsertChars;
    protected TextView mTextViewInsertNumber;
    protected TextView mTextViewInsertFileSize;

    protected MenuItem mMenuItemCriterionEdit;
    protected MenuItem mMenuItemCriterionDelete;
    protected MenuItem mMenuItemCriterionNext;

    protected FloatingActionButton mFloatingActionButtonGeneral;

    protected ViewPager mViewPagerContent;
    private boolean isOpen = false;
    private Long taskId = 0L;

    private CriterionAdapter mAdapter;
    private CriterionViewModel mViewModel;
    private boolean isEdit = false;
    private CriterionAdapter.DataHolder selectedCriterion = new CriterionAdapter.DataHolder();
    private Callbacks mCallback;

    private List<CriterionAdapter.DataHolder> dataHoldersList = new ArrayList<>();

    public RenameCriterionFragment(){
    }

    public static RenameCriterionFragment newInstance(long id){
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_TASK_ID, id);

        RenameCriterionFragment fragment = new RenameCriterionFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rename_criterion,container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            taskId = args.getLong(ARG_LONG_TASK_ID, 0);
        }

        ((RenameActivity) getActivity()).setTaskIdForPreview(taskId);

        initView();
        initViewModel();
        initAnimation();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_criterion_item, menu);
        mMenuItemCriterionEdit = menu.findItem(R.id.item_criterion_edit_button);
        mMenuItemCriterionDelete = menu.findItem(R.id.item_criterion_delete_button);
        mMenuItemCriterionNext = menu.findItem(R.id.item_criterion_next_button);

        uiButtonManagement();
    }

    private void initView(){
        mTextViewNoRenameCriterionCreated = getActivity().findViewById(R.id.textview_no_rename_criterion_created);
        mRecycleViewCriterionCreated = getActivity().findViewById(R.id.recyclerview_created_criterion);

        mFloatingActionButtonGeneral = getActivity().findViewById(R.id.floatingactionbutton_criterion_general);
        mTextViewReplaceChars = getActivity().findViewById(R.id.textview_replace_character);
        mTextViewNewName = getActivity().findViewById(R.id.textview_new_name);
        mTextViewInsertChars = getActivity().findViewById(R.id.textview_insert_character);
        mTextViewInsertNumber = getActivity().findViewById(R.id.textview_insert_number);
        mTextViewInsertFileSize = getActivity().findViewById(R.id.textview_insert_file_size);
        mViewPagerContent = getActivity().findViewById(R.id.viewpager_content);

        mFloatingActionButtonGeneral.setOnClickListener(this::fabGeneralOnClick);
        mTextViewReplaceChars.setOnClickListener(this::textViewReplaceCharOnClick);
        mTextViewNewName.setOnClickListener(this::textViewNewNameOnClick);
        mTextViewInsertChars.setOnClickListener(this::textViewInsertCharOnClick);
        mTextViewInsertNumber.setOnClickListener(this::textViewInsertNumberOnClick);
        mTextViewInsertFileSize.setOnClickListener(this::textViewInsertFileSizeOnClick);

        mViewPagerContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 1){
                    if(mCallback != null){
                        mCallback.onNextIconClicked(saveSelectedCriterionToList());
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mAdapter = new CriterionAdapter();
        mAdapter.setCallbacks(this);

        mRecycleViewCriterionCreated.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false));
        mRecycleViewCriterionCreated.setHasFixedSize(true);
        mRecycleViewCriterionCreated.setAdapter(mAdapter);
    }

    private void initViewModel(){
        mViewModel = new ViewModelProvider(this, new CriterionViewModelFactory(getActivity().getApplication())).get(CriterionViewModel.class);
        mViewModel.getCriterionAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<CriterionAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<CriterionAdapter.DataHolder> dataHolders) {
                dataHoldersList = dataHolders;
                mAdapter.setData(dataHoldersList);
                mAdapter.notifyDataSetChanged();

                if(dataHolders.isEmpty()){
                    mTextViewNoRenameCriterionCreated.setVisibility(View.VISIBLE);
                    mRecycleViewCriterionCreated.setVisibility(View.GONE);
                }else{
                    mTextViewNoRenameCriterionCreated.setVisibility(View.GONE);
                    mRecycleViewCriterionCreated.setVisibility(View.VISIBLE);
                }
            }
        });

        mViewModel.loadCriterionAdapterList(taskId);

        initAdapterDragging();
    }

    private void initAdapterDragging(){
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {

                int position_dragged = dragged.getAdapterPosition();
                int position_target = target.getAdapterPosition();

                Collections.swap(dataHoldersList, position_dragged, position_target);
                mAdapter.notifyItemMoved(position_dragged,position_target);

                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            }
        });

        mItemTouchHelper.attachToRecyclerView(mRecycleViewCriterionCreated);
    }

    private void initAnimation(){
        fabOpen = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_fade_in);
        fabClose = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_fade_out);
        fabClock = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_rotate_clock);
        fabAntiClock = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_rotate_anticlock);
    }

    private void fabGeneralOnClick(View view){
        if(isOpen){
            initFloatingItem(fabAntiClock, fabClose, false, View.INVISIBLE);
        }else{
            initFloatingItem(fabClock, fabOpen, true, View.VISIBLE);
        }
    }

    private void textViewReplaceCharOnClick(View view){
        View dialogReplaceCharTask = getLayoutInflater().inflate(R.layout.dialog_replace_char_task, null);
        EditText mEditTextCharToReplace = dialogReplaceCharTask.findViewById(R.id.edittext_character_to_replace);
        EditText mEditTextReplaceWith = dialogReplaceCharTask.findViewById(R.id.edittext_replace_with);
        MaterialCheckBox mMaterialCheckBox = dialogReplaceCharTask.findViewById(R.id.checkbox_match_case);

        if(isEdit){
            if(selectedCriterion.typeOfCriterion instanceof ReplaceCharacter){
                mEditTextCharToReplace.setText(((ReplaceCharacter) selectedCriterion.typeOfCriterion).getCharactersToReplace());
                mEditTextReplaceWith.setText(((ReplaceCharacter) selectedCriterion.typeOfCriterion).getReplaceWith());
                if(((ReplaceCharacter) selectedCriterion.typeOfCriterion).getMatchCases()){
                    mMaterialCheckBox.setChecked(true);
                }
            }
        }else{
            mMaterialCheckBox.setChecked(true);
        }

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(getString(R.string.add_replace_char_task))
                .setView(dialogReplaceCharTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!isEdit){
                            fabGeneralOnClick(view);
                        }
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String charToReplace = mEditTextCharToReplace.getText().toString();
                String replaceWith = mEditTextReplaceWith.getText().toString();

                if(replaceWith.matches(".*[\\\\/:*?\"<>|].*")){
                    mEditTextReplaceWith.setError(getString(R.string.error_msg_file_name_with_symbol));
                    return;
                }

                if(charToReplace.trim().equals("")){
                    mEditTextCharToReplace.setError(getString(R.string.error_msg_char_to_replace_empty));
                    return;
                }

                if(replaceWith.trim().equals("")){
                    mEditTextReplaceWith.setError(getString(R.string.error_msg_file_name_empty));
                    return;
                }

                if(isEdit){
                    if(selectedCriterion.typeOfCriterion instanceof ReplaceCharacter){
                        mViewModel.editReplaceCharacter(((ReplaceCharacter) selectedCriterion.typeOfCriterion).getId(), charToReplace, replaceWith, mMaterialCheckBox.isChecked(), taskId);
                        isEdit = false;
                    }
                    uiButtonManagement();
                }else{
                    mViewModel.addReplaceCharacter(charToReplace, replaceWith, mMaterialCheckBox.isChecked(), taskId);
                    fabGeneralOnClick(view);
                }
                dialog.dismiss();
            }
        });
    }

    private void textViewNewNameOnClick(View view){
        View dialogNewNameTask = getLayoutInflater().inflate(R.layout.dialog_new_name_task, null);
        EditText mEditTextNewName = dialogNewNameTask.findViewById(R.id.edittext_new_name);

        if(isEdit){
            if(selectedCriterion.typeOfCriterion instanceof NewName){
                mEditTextNewName.setText(((NewName) selectedCriterion.typeOfCriterion).getNewName());
            }
        }

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(getString(R.string.add_new_name_task))
                .setView(dialogNewNameTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!isEdit){
                            fabGeneralOnClick(view);
                        }
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newName = mEditTextNewName.getText().toString();

                if(newName.matches(".*[\\\\/:*?\"<>|].*")){
                    mEditTextNewName.setError(getString(R.string.error_msg_file_name_with_symbol));
                    return;
                }

                if(newName.trim().equals("")){
                    mEditTextNewName.setError(getString(R.string.error_msg_new_name_empty));
                    return;
                }

                if(isEdit){
                    if(selectedCriterion.typeOfCriterion instanceof NewName){
                        mViewModel.editNewName(((NewName) selectedCriterion.typeOfCriterion).getId(), newName, taskId);
                        isEdit = false;
                    }
                    uiButtonManagement();
                }else{
                    mViewModel.addNewName(newName, taskId);
                    fabGeneralOnClick(view);
                }

                dialog.dismiss();
            }
        });
    }

    private void textViewInsertCharOnClick(View view){
        View dialogInsertCharTask = getLayoutInflater().inflate(R.layout.dialog_insert_char_task,null);
        EditText mEditTextCharToInsert = dialogInsertCharTask.findViewById(R.id.edittext_character_to_insert);
        RadioGroup mRadioGroupInsertOption = dialogInsertCharTask.findViewById(R.id.radiogroup_insert_option);
        RelativeLayout mRelativeLayoutCustomPositionHiddenView = dialogInsertCharTask.findViewById(R.id.relativelayout_custom_position_hidden_view);
        EditText mEditTextPosition = dialogInsertCharTask.findViewById(R.id.edittext_position);
        MaterialCheckBox mMaterialCheckBoxBackward = dialogInsertCharTask.findViewById(R.id.checkbox_backward);

        if(isEdit){
            if(selectedCriterion.typeOfCriterion instanceof InsertCharacters){
                mEditTextCharToInsert.setText(((InsertCharacters) selectedCriterion.typeOfCriterion).getCharactersToInsert());

                switch (((InsertCharacters) selectedCriterion.typeOfCriterion).getInsertAtOption()){
                    case 0:
                        mRadioGroupInsertOption.check(R.id.radiobutton_before_name);
                        break;
                    case 1:
                        mRadioGroupInsertOption.check(R.id.radiobutton_after_name);
                        break;
                    case 2:
                        mRadioGroupInsertOption.check(R.id.radiobutton_custom_position);
                        break;
                }

                if(mRadioGroupInsertOption.getCheckedRadioButtonId() == R.id.radiobutton_custom_position){
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                    mEditTextPosition.setText(String.format("%d",((InsertCharacters) selectedCriterion.typeOfCriterion).getPosition()));
                    if(((InsertCharacters) selectedCriterion.typeOfCriterion).getBackward()){
                        mMaterialCheckBoxBackward.setChecked(true);
                    }
                }else{
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                }
            }
        }


        mRadioGroupInsertOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radiobutton_before_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_after_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_custom_position:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(getString(R.string.add_insert_char_task))
                .setView(dialogInsertCharTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!isEdit){
                            fabGeneralOnClick(view);
                        }
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String charToInsert = mEditTextCharToInsert.getText().toString();
                int selectedRadioButton = mRadioGroupInsertOption.getCheckedRadioButtonId();

                if(charToInsert.trim().equals("")){
                    mEditTextCharToInsert.setError(getString(R.string.error_msg_char_to_insert_empty));
                    return;
                }
                if(charToInsert.matches(".*[\\\\/:*?\"<>|].*")){
                    mEditTextCharToInsert.setError(getString(R.string.error_msg_file_name_with_symbol));
                    return;
                }

                int insertAtOption = 0;
                int position = 0;
                switch (selectedRadioButton) {
                    case R.id.radiobutton_before_name:
                        insertAtOption = 0;
                        break;
                    case R.id.radiobutton_after_name:
                        insertAtOption = 1;
                        break;
                    case R.id.radiobutton_custom_position:
                        if (mEditTextPosition.getText().toString().trim().equals("")) {
                            mEditTextPosition.setError(getString(R.string.error_msg_position_empty));
                            return;
                        }
                        position = Integer.parseInt(mEditTextPosition.getText().toString());
                        insertAtOption = 2;
                }

                if(isEdit){
                    if(selectedCriterion.typeOfCriterion instanceof InsertCharacters){
                        mViewModel.editInsertCharacters(((InsertCharacters) selectedCriterion.typeOfCriterion).getId(),
                                charToInsert, insertAtOption, position, mMaterialCheckBoxBackward.isChecked(), taskId);

                        isEdit = false;
                    }
                    uiButtonManagement();
                }else{
                    mViewModel.addInsertCharacters(charToInsert, insertAtOption, position, mMaterialCheckBoxBackward.isChecked(), taskId);
                    fabGeneralOnClick(view);
                }

                dialog.dismiss();
            }
        });
    }

    private void textViewInsertNumberOnClick(View view){
        View dialogInsertNumberTask = getLayoutInflater().inflate(R.layout.dialog_insert_number_task, null);
        EditText mEditTextNumberToInsert = dialogInsertNumberTask.findViewById(R.id.edittext_number_to_insert);
        MaterialCheckBox mMaterialCheckBoxAutoNumber = dialogInsertNumberTask.findViewById(R.id.checkbox_auto_number);
        RelativeLayout mRelativeLayoutAutoNumberHiddenView = dialogInsertNumberTask.findViewById(R.id.relativelayout_auto_number_hidden_view);
        Spinner mSpinnerIncrementDecrement = dialogInsertNumberTask.findViewById(R.id.spinner_increment_decrement);
        EditText mEditTextIncrementDecrementValue = dialogInsertNumberTask.findViewById(R.id.edittext_increment_decrement_value);
        RadioGroup mRadioGroupInsertOption = dialogInsertNumberTask.findViewById(R.id.radiogroup_insert_option);
        RelativeLayout mRelativeLayoutCustomPositionHiddenView = dialogInsertNumberTask.findViewById(R.id.relativelayout_custom_position_hidden_view);
        EditText mEditTextPosition = dialogInsertNumberTask.findViewById(R.id.edittext_position);
        MaterialCheckBox mMaterialCheckBoxBackward = dialogInsertNumberTask.findViewById(R.id.checkbox_backward);

        if(isEdit){
            if(selectedCriterion.typeOfCriterion instanceof InsertNumber){
                mEditTextNumberToInsert.setText(((InsertNumber) selectedCriterion.typeOfCriterion).getNumbersToInsert());
                if(((InsertNumber) selectedCriterion.typeOfCriterion).getAutoNumber()){
                    mMaterialCheckBoxAutoNumber.setChecked(true);
                    mRelativeLayoutAutoNumberHiddenView.setVisibility(View.VISIBLE);
                    mSpinnerIncrementDecrement.setSelection(((InsertNumber) selectedCriterion.typeOfCriterion).getIncrementDecrementOption());
                    mEditTextIncrementDecrementValue.setText(String.format("%d", ((InsertNumber) selectedCriterion.typeOfCriterion).getIncrementDecrementValue()));
                }

                switch(((InsertNumber) selectedCriterion.typeOfCriterion).getInsertAtOption()){
                    case 0:
                        mRadioGroupInsertOption.check(R.id.radiobutton_before_name);
                        break;
                    case 1:
                        mRadioGroupInsertOption.check(R.id.radiobutton_after_name);
                        break;
                    case 2:
                        mRadioGroupInsertOption.check(R.id.radiobutton_custom_position);
                        break;
                }

                if(mRadioGroupInsertOption.getCheckedRadioButtonId() == R.id.radiobutton_custom_position){
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                    mEditTextPosition.setText(String.format("%d",((InsertNumber) selectedCriterion.typeOfCriterion).getPosition()));
                    if(((InsertNumber) selectedCriterion.typeOfCriterion).getBackward()){
                        mMaterialCheckBoxBackward.setChecked(true);
                    }
                }else{
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                }
            }
        }

        mMaterialCheckBoxAutoNumber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mRelativeLayoutAutoNumberHiddenView.setVisibility(View.VISIBLE);
                }else{
                    mRelativeLayoutAutoNumberHiddenView.setVisibility(View.GONE);
                }
            }
        });

        mSpinnerIncrementDecrement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        mEditTextIncrementDecrementValue.setHint(R.string.placeholder_increment_value);
                        break;
                    case 1:
                        mEditTextIncrementDecrementValue.setHint(R.string.placeholder_decrement_value);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mRadioGroupInsertOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radiobutton_before_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_after_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_custom_position:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(getString(R.string.add_insert_number_task))
                .setView(dialogInsertNumberTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!isEdit){
                            fabGeneralOnClick(view);
                        }
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberToInsert = mEditTextNumberToInsert.getText().toString();
                int selectedFileRadioButton = mRadioGroupInsertOption.getCheckedRadioButtonId();

                int inDecrementValue = 0;
                int insertAtOption = 0;
                int positionInInt = 0;
                boolean isAutoNumber = false;

                if(numberToInsert.trim().equals("")){
                    mEditTextNumberToInsert.setError(getString(R.string.error_msg_number_to_insert_empty));
                    return;
                }

                if(mMaterialCheckBoxAutoNumber.isChecked()){
                    String incrementDecrementValue = mEditTextIncrementDecrementValue.getText().toString();
                    isAutoNumber = true;

                    if(incrementDecrementValue.trim().equals("")){
                        switch (mSpinnerIncrementDecrement.getSelectedItemPosition()){
                            case 0:
                                mEditTextIncrementDecrementValue.setError(getString(R.string.error_msg_increment_value_empty));
                                break;
                            case 1:
                                mEditTextIncrementDecrementValue.setError(getString(R.string.error_msg_decrement_value_empty));
                                break;
                        }
                        return;
                    }

                    inDecrementValue = Integer.parseInt(incrementDecrementValue);
                }

                switch (selectedFileRadioButton) {
                    case R.id.radiobutton_before_name:
                        insertAtOption = 0;
                        break;
                    case R.id.radiobutton_after_name:
                        insertAtOption = 1;
                        break;
                    case R.id.radiobutton_custom_position:
                        if (mEditTextPosition.getText().toString().trim().equals("")) {
                            mEditTextPosition.setError(getString(R.string.error_msg_position_empty));
                            return;
                        }
                        positionInInt = Integer.parseInt(mEditTextPosition.getText().toString());
                        insertAtOption = 2;
                }

                if(isEdit){
                    if(selectedCriterion.typeOfCriterion instanceof InsertNumber){
                        mViewModel.editInsertNumber(((InsertNumber) selectedCriterion.typeOfCriterion).getId(),
                                numberToInsert, isAutoNumber, mSpinnerIncrementDecrement.getSelectedItemPosition(), inDecrementValue, insertAtOption,
                                positionInInt, mMaterialCheckBoxBackward.isChecked(), taskId);

                        isEdit = false;
                        uiButtonManagement();
                    }
                }else{
                    mViewModel.addInsertNumber(numberToInsert, isAutoNumber,
                            mSpinnerIncrementDecrement.getSelectedItemPosition(), inDecrementValue, insertAtOption, positionInInt, mMaterialCheckBoxBackward.isChecked(), taskId);
                    fabGeneralOnClick(view);
                }

                dialog.dismiss();
            }
        });


    }

    private void textViewInsertFileSizeOnClick(View view){
        View dialogInsertFileSizeTask = getLayoutInflater().inflate(R.layout.dialog_insert_file_size_task,null);
        Spinner mSpinnerFileSizeFormat = dialogInsertFileSizeTask.findViewById(R.id.spinner_file_size_format);
        EditText mEditTextDecimalPlaces = dialogInsertFileSizeTask.findViewById(R.id.edittext_decimal_places);
        MaterialCheckBox mMaterialCheckBoxShowUnit = dialogInsertFileSizeTask.findViewById(R.id.checkbox_show_unit);
        RadioGroup mRadioGroupInsertOption = dialogInsertFileSizeTask.findViewById(R.id.radiogroup_insert_option);
        RelativeLayout mRelativeLayoutCustomPositionHiddenView = dialogInsertFileSizeTask.findViewById(R.id.relativelayout_custom_position_hidden_view);
        EditText mEditTextPosition = dialogInsertFileSizeTask.findViewById(R.id.edittext_position);
        MaterialCheckBox mMaterialCheckBoxBackward = dialogInsertFileSizeTask.findViewById(R.id.checkbox_backward);

        if(isEdit){
            if(selectedCriterion.typeOfCriterion instanceof InsertFileSize){
                mSpinnerFileSizeFormat.setSelection(((InsertFileSize) selectedCriterion.typeOfCriterion).getFormatSizeOption());
                mEditTextDecimalPlaces.setText(String.format("%d", ((InsertFileSize) selectedCriterion.typeOfCriterion).getDecimalPlaces()));
                if(((InsertFileSize) selectedCriterion.typeOfCriterion).getShowUnit()){
                    mMaterialCheckBoxShowUnit.setChecked(true);
                }

                switch (((InsertFileSize) selectedCriterion.typeOfCriterion).getInsertAtOption()){
                    case 0:
                        mRadioGroupInsertOption.check(R.id.radiobutton_before_name);
                        break;
                    case 1:
                        mRadioGroupInsertOption.check(R.id.radiobutton_after_name);
                        break;
                    case 2:
                        mRadioGroupInsertOption.check(R.id.radiobutton_custom_position);
                        break;
                }

                if(mRadioGroupInsertOption.getCheckedRadioButtonId() == R.id.radiobutton_custom_position){
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                    mEditTextPosition.setText(String.format("%d", ((InsertFileSize) selectedCriterion.typeOfCriterion).getPosition()));
                    if(((InsertFileSize) selectedCriterion.typeOfCriterion).getBackward()){
                        mMaterialCheckBoxBackward.setChecked(true);
                    }
                }else{
                    mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                }

            }
        }else{
            mMaterialCheckBoxShowUnit.setChecked(true);
        }

        mRadioGroupInsertOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radiobutton_before_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_after_name:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.GONE);
                        break;
                    case R.id.radiobutton_custom_position:
                        mRelativeLayoutCustomPositionHiddenView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(R.string.add_insert_file_size_task)
                .setView(dialogInsertFileSizeTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!isEdit){
                            fabGeneralOnClick(view);
                        }
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String decimalPlaces = mEditTextDecimalPlaces.getText().toString();
                int selectedRadioButton = mRadioGroupInsertOption.getCheckedRadioButtonId();

                if(decimalPlaces.trim().equals("")){
                    mEditTextDecimalPlaces.setError(getString(R.string.error_msg_decimal_place_empty));
                    return;
                }

                int decimalPlacesInInt = Integer.parseInt(decimalPlaces);

                if(decimalPlacesInInt < 0 | decimalPlacesInInt > 6){
                    mEditTextDecimalPlaces.setError(getString(R.string.error_msg_decimal_place_range));
                    return;
                }

                int insertAtOption = 0;
                int index = 0;

                int formatSelected = mSpinnerFileSizeFormat.getSelectedItemPosition();

                switch (selectedRadioButton) {
                    case R.id.radiobutton_before_name:
                        insertAtOption = 0;
                        break;
                    case R.id.radiobutton_after_name:
                        insertAtOption = 1;
                        break;
                    case R.id.radiobutton_custom_position:
                        if (mEditTextPosition.getText().toString().trim().equals("")) {
                            mEditTextPosition.setError(getString(R.string.error_msg_position_empty));
                            return;
                        }
                        index = Integer.parseInt(mEditTextPosition.getText().toString());
                        insertAtOption = 2;
                }

                if(isEdit){
                    if(selectedCriterion.typeOfCriterion instanceof InsertFileSize){
                        mViewModel.editInsertFileSize(((InsertFileSize) selectedCriterion.typeOfCriterion).getId(),
                                formatSelected, Integer.parseInt(decimalPlaces), mMaterialCheckBoxShowUnit.isChecked(), insertAtOption, index,
                                mMaterialCheckBoxBackward.isChecked(), taskId);

                        isEdit = false;
                    }
                    uiButtonManagement();
                }else{
                    mViewModel.addInsertFileSize(formatSelected, Integer.parseInt(decimalPlaces),
                            mMaterialCheckBoxShowUnit.isChecked(), insertAtOption, index, mMaterialCheckBoxBackward.isChecked(), taskId);

                    fabGeneralOnClick(view);
                }

                dialog.dismiss();
            }
        });
    }

    private void initFloatingItem(Animation generalFabAnimation, Animation otherFabAnimation, boolean clickable, int viewVisibility){
        mFloatingActionButtonGeneral.setAnimation(generalFabAnimation);


        mTextViewReplaceChars.setAnimation(otherFabAnimation);
        mTextViewNewName.setAnimation(otherFabAnimation);
        mTextViewInsertChars.setAnimation(otherFabAnimation);
        mTextViewInsertNumber.setAnimation(otherFabAnimation);
        mTextViewInsertFileSize.setAnimation(otherFabAnimation);


        mTextViewReplaceChars.setVisibility(viewVisibility);
        mTextViewNewName.setVisibility(viewVisibility);
        mTextViewInsertChars.setVisibility(viewVisibility);
        mTextViewInsertNumber.setVisibility(viewVisibility);
        mTextViewInsertFileSize.setVisibility(viewVisibility);

        mTextViewReplaceChars.setClickable(clickable);
        mTextViewNewName.setClickable(clickable);
        mTextViewInsertChars.setClickable(clickable);
        mTextViewInsertNumber.setClickable(clickable);
        mTextViewInsertFileSize.setClickable(clickable);
        isOpen = clickable;
    }

    private void uiButtonManagement(){
        if(mAdapter.getSelectedCriterion().size() == 0){
            mMenuItemCriterionEdit.setVisible(false);
            mMenuItemCriterionDelete.setVisible(false);
            mMenuItemCriterionNext.setVisible(false);

            mFloatingActionButtonGeneral.show();
            mFloatingActionButtonGeneral.setClickable(true);
        }else{
            if(mAdapter.getSelectedCriterion().size() == 1){
                mMenuItemCriterionEdit.setVisible(true);
            }else{
                mMenuItemCriterionEdit.setVisible(false);
            }
            mMenuItemCriterionDelete.setVisible(true);
            mMenuItemCriterionNext.setVisible(true);

            if(isOpen){
                fabGeneralOnClick(getView());
            }
            mFloatingActionButtonGeneral.hide();
            mFloatingActionButtonGeneral.setClickable(false);
        }
    }

    @Override
    public void onListItemClicked(CriterionAdapter.DataHolder data, ImageView imageViewTick) {
        if(data.isChecked){
            mViewModel.editCriterion(data.id, taskId, false);
            imageViewTick.setVisibility(View.GONE);
        }else{
            mViewModel.editCriterion(data.id, taskId, true);
            imageViewTick.setVisibility(View.VISIBLE);
        }

        uiButtonManagement();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(mAdapter.getSelectedCriterion() != null){
            switch (item.getItemId()){
                case R.id.item_criterion_edit_button:
                    selectedCriterion = mAdapter.getSelectedCriterion().get(0);

                    if(selectedCriterion.typeOfCriterion instanceof ReplaceCharacter){
                        isEdit = true;
                        textViewReplaceCharOnClick(getView());
                    }else if(selectedCriterion.typeOfCriterion instanceof NewName){
                        isEdit = true;
                        textViewNewNameOnClick(getView());
                    }else if(selectedCriterion.typeOfCriterion instanceof InsertCharacters){
                        isEdit = true;
                        textViewInsertCharOnClick(getView());
                    }else if(selectedCriterion.typeOfCriterion instanceof InsertNumber){
                        isEdit = true;
                        textViewInsertNumberOnClick(getView());
                    }else if(selectedCriterion.typeOfCriterion instanceof InsertFileSize){
                        isEdit = true;
                        textViewInsertFileSizeOnClick(getView());
                    }
                    break;
                case R.id.item_criterion_delete_button:
                    new MaterialAlertDialogBuilder(getActivity())
                            .setTitle(getString(R.string.dialog_title_delete))
                            .setMessage(getString(R.string.dialog_message_delete))
                            .setPositiveButton(getString(R.string.yes_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    List<CriterionAdapter.DataHolder> selectedCriterionList = new ArrayList<>();
                                    selectedCriterionList = mAdapter.getSelectedCriterion();

                                    for(int i=0 ; i < selectedCriterionList.size(); i++){
                                        if(selectedCriterionList.get(i).typeOfCriterion instanceof ReplaceCharacter){
                                            mViewModel.deleteReplaceCharacter(((ReplaceCharacter) selectedCriterionList.get(i).typeOfCriterion).getId(), "RC" , taskId);
                                        }else if(selectedCriterionList.get(i).typeOfCriterion instanceof NewName){
                                            mViewModel.deleteNewName(((NewName) selectedCriterionList.get(i).typeOfCriterion).getId(), "NN",taskId);
                                        }else if(selectedCriterionList.get(i).typeOfCriterion instanceof InsertCharacters){
                                            mViewModel.deleteInsertCharacters(((InsertCharacters) selectedCriterionList.get(i).typeOfCriterion).getId(),"IC",taskId);
                                        }else if(selectedCriterionList.get(i).typeOfCriterion instanceof InsertNumber){
                                            mViewModel.deleteInsertNumber(((InsertNumber) selectedCriterionList.get(i).typeOfCriterion).getId(), "IN",taskId);
                                        }else if(selectedCriterionList.get(i).typeOfCriterion instanceof InsertFileSize){
                                            mViewModel.deleteInsertFileSize(((InsertFileSize) selectedCriterionList.get(i).typeOfCriterion).getId(), "IFS",taskId);
                                        }
                                    }

                                    mMenuItemCriterionEdit.setVisible(false);
                                    mMenuItemCriterionDelete.setVisible(false);
                                    mMenuItemCriterionNext.setVisible(false);
                                    mFloatingActionButtonGeneral.show();
                                    mFloatingActionButtonGeneral.setClickable(true);
                                }
                            })
                            .setNegativeButton(getString(R.string.no_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                    break;
                case R.id.item_criterion_next_button:
                    if(mCallback != null){
                        mCallback.onNextIconClicked(saveSelectedCriterionToList());
                    }
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private List<Object> saveSelectedCriterionToList(){
        List<Object> selectedCriterionList = new ArrayList<>();
        List<Long> criterionBridgeId = new ArrayList<>();

        for(int i=0; i<mAdapter.getSelectedCriterion().size(); i++){
            selectedCriterionList.add(mAdapter.getSelectedCriterion().get(i).typeOfCriterion);
            criterionBridgeId.add(mAdapter.getSelectedCriterion().get(i).id);
        }

        ((RenameActivity) getActivity()).setCriterionBridgeId(criterionBridgeId);

        return selectedCriterionList;
    }

    public void setCallback(Callbacks callback){
        mCallback = callback;
    }

    public interface Callbacks{
        public void onNextIconClicked(List<Object> selectedCriterionList);
    }
}
