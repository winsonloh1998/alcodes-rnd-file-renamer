package com.android.alcodes_rnd_file_renamer.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.activities.RenameActivity;
import com.android.alcodes_rnd_file_renamer.adapters.TaskAdapter;
import com.android.alcodes_rnd_file_renamer.database.entities.File;

import com.android.alcodes_rnd_file_renamer.utils.SharedPreferenceHelper;
import com.android.alcodes_rnd_file_renamer.viewmodels.Task.TaskViewModel;
import com.android.alcodes_rnd_file_renamer.viewmodels.Task.TaskViewModelFactory;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class RenameTaskFragment extends Fragment implements TaskAdapter.Callbacks{
    public static final String TAG = RenameTaskFragment.class.getSimpleName();
    private static final String OUTSTATE_SELECTED_TASK_LIST = "OUTSTATE_SELECTED_TASK_LIST";

    protected TextView mTextViewNoTaskCreated;

    protected FloatingActionButton mFloatingActionButtonGeneral;

    protected RecyclerView mRecyclerViewCreatedTask;

    private TaskAdapter mAdapter;
    private TaskViewModel mViewModel;

    protected MenuItem mMenuItemTaskEdit;
    protected MenuItem mMenuItemTaskDelete;
    protected MenuItem mMenuItemTaskNext;

    private boolean isEdit = false;

    public RenameTaskFragment(){
    }

    public static RenameTaskFragment newInstance(){
        return new RenameTaskFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rename_task, container, false);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_task_item, menu);
        mMenuItemTaskEdit = menu.findItem(R.id.item_task_edit_button);
        mMenuItemTaskDelete = menu.findItem(R.id.item_task_delete_button);
        mMenuItemTaskNext = menu.findItem(R.id.item_task_next_button);

        if(mAdapter.getSelectedTask().size() > 0){
            if(mAdapter.getSelectedTask().size() == 1){
                mMenuItemTaskEdit.setVisible(true);
                mMenuItemTaskNext.setVisible(true);
            }else{
                mMenuItemTaskEdit.setVisible(false);
                mMenuItemTaskNext.setVisible(false);
            }
            mMenuItemTaskDelete.setVisible(true);
            mFloatingActionButtonGeneral.hide();
        }else{
            mFloatingActionButtonGeneral.show();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initViewModel();
    }

    private void initView(){
        mTextViewNoTaskCreated = getActivity().findViewById(R.id.textview_no_rename_task_created);
        mFloatingActionButtonGeneral = getActivity().findViewById(R.id.floatingactionbutton_task_general);
        mRecyclerViewCreatedTask = getActivity().findViewById(R.id.recyclerview_created_task);

        mFloatingActionButtonGeneral.setOnClickListener(this::fabGeneralOnClick);

        mAdapter = new TaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerViewCreatedTask.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerViewCreatedTask.setHasFixedSize(true);
        mRecyclerViewCreatedTask.setAdapter(mAdapter);
    }

    private void initViewModel(){
        mViewModel = new ViewModelProvider(this, new TaskViewModelFactory(getActivity().getApplication())).get(TaskViewModel.class);
        mViewModel.getTaskAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<TaskAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<TaskAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                if(dataHolders.isEmpty()){
                    mTextViewNoTaskCreated.setVisibility(View.VISIBLE);
                    mRecyclerViewCreatedTask.setVisibility(View.GONE);
                }else{
                    mTextViewNoTaskCreated.setVisibility(View.GONE);
                    mRecyclerViewCreatedTask.setVisibility(View.VISIBLE);
                }
            }
        });

        mViewModel.loadTaskAdapterList();
    }

    private void fabGeneralOnClick(View view){
        View dialogCreateTask = getLayoutInflater().inflate(R.layout.dialog_create_task, null);
        EditText mEditTextTaskName = dialogCreateTask.findViewById(R.id.edittext_task_name);

        if(isEdit){
            mEditTextTaskName.setText(mAdapter.getSelectedTask().get(0).taskName);
        }

        AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(getString(R.string.title_dialog_create_task))
                .setView(dialogCreateTask)
                .setPositiveButton(getString(R.string.ok_button), null)
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isEdit = false;
                        dialog.dismiss();
                    }
                })
                .show();

        Button positiveDialogButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taskName = mEditTextTaskName.getText().toString();
                if(taskName.trim().equals("")){
                    mEditTextTaskName.setError(getString(R.string.error_msg_task_name_empty));
                    return;
                }

                if(isEdit){
                    mViewModel.editTask(mAdapter.getSelectedTask().get(0).id, taskName);
                    isEdit = false;

                    mMenuItemTaskEdit.setVisible(false);
                    mMenuItemTaskDelete.setVisible(false);
                    mMenuItemTaskNext.setVisible(false);
                    mFloatingActionButtonGeneral.show();
                }else{
                    mViewModel.addTask(taskName);
                }


                dialog.dismiss();
            }
        });
    }

    @Override
    public void onListItemClicked(TaskAdapter.DataHolder data, ImageView imageViewTick) {
        if(data.isChecked){
            mViewModel.editIsChecked(data.id, false);
            imageViewTick.setVisibility(View.GONE);
        }else{
            mViewModel.editIsChecked(data.id, true);
            imageViewTick.setVisibility(View.VISIBLE);
        }

        if(mAdapter.getSelectedTask().size() == 0){
            mMenuItemTaskEdit.setVisible(false);
            mMenuItemTaskDelete.setVisible(false);
            mMenuItemTaskNext.setVisible(false);
            mFloatingActionButtonGeneral.show();
        }else{
            if(mAdapter.getSelectedTask().size() == 1){
                mMenuItemTaskEdit.setVisible(true);
                mMenuItemTaskNext.setVisible(true);
            }else{
                mMenuItemTaskEdit.setVisible(false);
                mMenuItemTaskNext.setVisible(false);
            }
            mMenuItemTaskDelete.setVisible(true);
            mFloatingActionButtonGeneral.hide();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(mAdapter.getSelectedTask() != null){
            switch (item.getItemId()){
                case R.id.item_task_edit_button:
                    isEdit = true;
                    fabGeneralOnClick(getView());
                    break;
                case R.id.item_task_delete_button:
                    new MaterialAlertDialogBuilder(getActivity())
                            .setTitle(getString(R.string.dialog_title_delete))
                            .setMessage(getString(R.string.dialog_message_delete))
                            .setPositiveButton(getString(R.string.yes_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    List<TaskAdapter.DataHolder> selectedTaskList = new ArrayList<>();
                                    selectedTaskList = mAdapter.getSelectedTask();

                                    for(int i=0; i < selectedTaskList.size(); i++){
                                        mViewModel.deleteTask(mAdapter.getSelectedTask().get(i).id);
                                    }
                                    mViewModel.loadTaskAdapterList();

                                    mMenuItemTaskEdit.setVisible(false);
                                    mMenuItemTaskDelete.setVisible(false);
                                    mMenuItemTaskNext.setVisible(false);
                                    mFloatingActionButtonGeneral.show();
                                }
                            })
                            .setNegativeButton(getString(R.string.no_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                    break;
                case R.id.item_task_next_button:
                    Long taskId = mAdapter.getSelectedTask().get(0).id;
                    Intent intent = new Intent(getActivity(), RenameActivity.class);
                    intent.putExtra(RenameActivity.EXTRA_LONG_TASK_ID, taskId);
                    startActivity(intent);
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
