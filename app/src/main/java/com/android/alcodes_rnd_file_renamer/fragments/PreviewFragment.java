package com.android.alcodes_rnd_file_renamer.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.activities.MainActivity;
import com.android.alcodes_rnd_file_renamer.activities.RenameActivity;
import com.android.alcodes_rnd_file_renamer.adapters.PreviewAdapter;
import com.android.alcodes_rnd_file_renamer.database.entities.File;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertCharacters;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.android.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.android.alcodes_rnd_file_renamer.database.entities.NewName;
import com.android.alcodes_rnd_file_renamer.database.entities.ReplaceCharacter;
import com.android.alcodes_rnd_file_renamer.utils.CriterionHelper;
import com.android.alcodes_rnd_file_renamer.utils.SharedPreferenceHelper;
import com.android.alcodes_rnd_file_renamer.utils.UriDeserializerHelper;
import com.android.alcodes_rnd_file_renamer.viewmodels.Criterion.CriterionViewModel;
import com.android.alcodes_rnd_file_renamer.viewmodels.Criterion.CriterionViewModelFactory;
import com.android.alcodes_rnd_file_renamer.viewmodels.Task.TaskViewModel;
import com.android.alcodes_rnd_file_renamer.viewmodels.Task.TaskViewModelFactory;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class PreviewFragment extends Fragment{
    private static final int OPEN_FOLDER_REQUEST_CODE = 40;

    protected RecyclerView mRecyclerViewPreviewName;
    protected TextView mTextViewNoPreviewName;
    protected RelativeLayout mRelativeLayoutPreviewPage;

    protected MenuItem mMenuItemPreviewNextButton;

    private List<File> fileList = new ArrayList<>();
    private List<Object> criterionList = new ArrayList<>();
    private List<PreviewAdapter.DataHolder> previewList = new ArrayList<>();
    private List<String> fileUriAfterRenamed = new ArrayList<>();;

    private boolean isGoingToRename = false;
    private boolean isRenameSuccessful = false;

    private PreviewAdapter mAdapter;
    private Uri targetParentUri = null;

    private AlertDialog confirmationDialog;
    private View dialogRenameConfirmation;
    private MaterialCheckBox mMaterialCheckBoxRenameAndMove;
    private LinearLayout mRelativeLayoutConfirmationHiddenView;
    private TextView mTextViewDirPath;
    private MaterialButton mMaterialButtonFindPath;
    private Button mPositiveDialogButton;

    public PreviewFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview_name,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initSelectedFile();
        initView();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        initPreviewView();

        inflater.inflate(R.menu.menu_preview_item, menu);
        mMenuItemPreviewNextButton = menu.findItem(R.id.item_preview_next_button);

        if(fileList.size() > 0 && criterionList.size() > 0){
            mMenuItemPreviewNextButton.setVisible(true);
        }else{
            mMenuItemPreviewNextButton.setVisible(false);
        }
    }

    private void initSelectedFile(){
        Type fileListType = new TypeToken<List<File>>(){}.getType();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class ,new UriDeserializerHelper())
                .create();

        if(SharedPreferenceHelper.getInstance(getActivity()).getString("selectedFiles", "") != null){
            fileList = gson.fromJson(SharedPreferenceHelper.getInstance(getActivity())
                    .getString("selectedFiles", ""),fileListType);
        }
    }

    private void conductRenameToFile(){
        //Refresh to Prevent Stacking Up More and More
        previewList = new ArrayList<>();

        CriterionHelper criterionHelper = new CriterionHelper();
        criterionList = ((RenameActivity) getActivity()).getSelectedCriterionList();

        for(int i=0; i < fileList.size(); i++) {
            PreviewAdapter.DataHolder dataHolder = new PreviewAdapter.DataHolder();
            dataHolder.beforeRename = fileList.get(i).getFileName() + "." + fileList.get(i).getFileType();

            if(criterionList.size() != 0){
                String newName = fileList.get(i).getFileName();
                String tempName = "";

                for(int j=0; j < criterionList.size(); j++){

                    if(criterionList.get(j) instanceof ReplaceCharacter){
                        tempName = criterionHelper.replaceCharacterCriterion(newName,
                                ((ReplaceCharacter) criterionList.get(j)).getCharactersToReplace(),
                                ((ReplaceCharacter) criterionList.get(j)).getReplaceWith(),
                                ((ReplaceCharacter) criterionList.get(j)).getMatchCases());
                    }else if(criterionList.get(j) instanceof NewName){
                        tempName = criterionHelper.newNameCriterion(((NewName) criterionList.get(j)).getNewName(), i);
                    }else if(criterionList.get(j) instanceof InsertCharacters){
                        tempName = criterionHelper.insertCharactersCriterion(newName,
                                ((InsertCharacters) criterionList.get(j)).getCharactersToInsert(),
                                ((InsertCharacters) criterionList.get(j)).getInsertAtOption(),
                                ((InsertCharacters) criterionList.get(j)).getPosition(),
                                ((InsertCharacters) criterionList.get(j)).getBackward());
                    }else if(criterionList.get(j) instanceof InsertNumber){
                        tempName = criterionHelper.insertNumberCriterion(newName,
                                ((InsertNumber) criterionList.get(j)).getNumbersToInsert(),
                                ((InsertNumber) criterionList.get(j)).getAutoNumber(),
                                ((InsertNumber) criterionList.get(j)).getIncrementDecrementOption(),
                                ((InsertNumber) criterionList.get(j)).getIncrementDecrementValue(),
                                ((InsertNumber) criterionList.get(j)).getInsertAtOption(),
                                ((InsertNumber) criterionList.get(j)).getPosition(),
                                ((InsertNumber) criterionList.get(j)).getBackward(), i);
                    }else if(criterionList.get(j) instanceof InsertFileSize){
                        tempName = criterionHelper.insertFileSize(newName, Double.valueOf(fileList.get(i).getFileSize()),
                                ((InsertFileSize) criterionList.get(j)).getFormatSizeOption(),
                                ((InsertFileSize) criterionList.get(j)).getDecimalPlaces(),
                                ((InsertFileSize) criterionList.get(j)).getShowUnit(),
                                ((InsertFileSize) criterionList.get(j)).getInsertAtOption(),
                                ((InsertFileSize) criterionList.get(j)).getPosition(),
                                ((InsertFileSize) criterionList.get(j)).getBackward());
                    }

                    newName = tempName;
                }
                newName = newName + "." + fileList.get(i).getFileType();
                dataHolder.afterRename = newName;

                if(isGoingToRename){
                    try{
                        fileUriAfterRenamed.add(renameTo(getActivity().getApplicationContext(),
                                fileList.get(i).getFileUri(),
                                newName).toString());
                        isRenameSuccessful = true;
                    }catch (Exception ex){
                        isRenameSuccessful = false;
                        ex.printStackTrace();
                    }
                }
            }else{
                dataHolder.afterRename = fileList.get(i).getFileName() + "." + fileList.get(i).getFileType();;
            }
            previewList.add(dataHolder);
        }

        isGoingToRename = false;
    }

    private void initPreviewView(){
        conductRenameToFile();

        mAdapter.setData(previewList);
        mAdapter.notifyDataSetChanged();

        if(previewList.size() != 0){
            mRecyclerViewPreviewName.setVisibility(View.VISIBLE);
            mTextViewNoPreviewName.setVisibility(View.GONE);
        }else{
            mRecyclerViewPreviewName.setVisibility(View.GONE);
            mTextViewNoPreviewName.setVisibility(View.VISIBLE);
        }
    }

    private void initView(){
        mRecyclerViewPreviewName = getActivity().findViewById(R.id.recyclerview_preview_name);
        mTextViewNoPreviewName = getActivity().findViewById(R.id.textview_no_preview_name);
        mRelativeLayoutPreviewPage = getActivity().findViewById(R.id.relativelayout_preview_page);

        mAdapter = new PreviewAdapter();

        mRecyclerViewPreviewName.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false));
        mRecyclerViewPreviewName.setHasFixedSize(true);
        mRecyclerViewPreviewName.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_preview_next_button:
                dialogRenameConfirmation = getLayoutInflater().inflate(R.layout.dialog_rename_confirmation, null);
                mMaterialCheckBoxRenameAndMove = dialogRenameConfirmation.findViewById(R.id.checkbox_rename_and_move_file);
                mRelativeLayoutConfirmationHiddenView = dialogRenameConfirmation.findViewById(R.id.relativelayout_confirmation_hidden_view);
                mTextViewDirPath = dialogRenameConfirmation.findViewById(R.id.textview_directory_path);
                mMaterialButtonFindPath = dialogRenameConfirmation.findViewById(R.id.button_open_file_picker);

                mMaterialCheckBoxRenameAndMove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked){
                            mRelativeLayoutConfirmationHiddenView.setVisibility(View.VISIBLE);
                        }else{
                            mRelativeLayoutConfirmationHiddenView.setVisibility(View.GONE);
                        }
                    }
                });

                confirmationDialog = new MaterialAlertDialogBuilder(getActivity())
                        .setTitle(getString(R.string.title_dialog_rename_confirmation))
                        .setView(dialogRenameConfirmation)
                        .setPositiveButton(getString(R.string.yes_button), null)
                        .setNegativeButton(getString(R.string.no_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                mMaterialButtonFindPath.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        startActivityForResult(intent, OPEN_FOLDER_REQUEST_CODE);
                        confirmationDialog.dismiss();
                    }
                });

                moveDocumentAndStartActivity();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == OPEN_FOLDER_REQUEST_CODE){
                if(data.getData() != null){
                    DocumentFile documentFile = DocumentFile.fromTreeUri(getActivity().getApplication(), data.getData());
                    targetParentUri = documentFile.getUri();
                    mTextViewDirPath.setText(targetParentUri.getPath());
                    moveDocumentAndStartActivity();
                }
            }
        }
    }

    private void moveDocumentAndStartActivity(){
        confirmationDialog.show();

        mPositiveDialogButton = confirmationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        mPositiveDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGoingToRename = true;
                conductRenameToFile();
                if(mMaterialCheckBoxRenameAndMove.isChecked()){
                    if(fileUriAfterRenamed != null){
                        for(int i=0; i<fileUriAfterRenamed.size(); i++){
                            try {
                                Uri newUri = Uri.parse(fileUriAfterRenamed.get(i));
                                moveTo(getActivity().getContentResolver(), newUri, newUri, targetParentUri);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    fileUriAfterRenamed = new ArrayList<>();
                }

                String message;
                if(isRenameSuccessful){
                    message = getString(R.string.rename_successful_message);
                }else{
                    message = getString(R.string.rename_unsuccessful_message);
                }

                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                confirmationDialog.dismiss();

                removeTaskAndCriterionIsChecked();

                MainActivity.currentMainActivity.finish();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    public static Uri renameTo(Context context, Uri self, String displayName) throws FileNotFoundException {
        return DocumentsContract.renameDocument(context.getContentResolver(), self, displayName);
    }

    public static Uri moveTo(ContentResolver contentResolver, Uri sourceDocUri, Uri sourceParentDocUri, Uri targetParentDocUri) throws FileNotFoundException{
        return DocumentsContract.moveDocument(contentResolver, sourceDocUri, sourceParentDocUri, targetParentDocUri);
    }

    private void removeTaskAndCriterionIsChecked(){
        TaskViewModel mTaskViewModel = new ViewModelProvider(this, new TaskViewModelFactory(getActivity().getApplication())).get(TaskViewModel.class);
        mTaskViewModel.editIsChecked(((RenameActivity) getActivity()).getTaskIdForPreview(),false);

        List<Long> criterionBridgeId = new ArrayList<>();
        criterionBridgeId = ((RenameActivity) getActivity()).getCriterionBridgeId();

        if(criterionBridgeId != null){
            CriterionViewModel mCriterionViewModel = new ViewModelProvider(this, new CriterionViewModelFactory(getActivity().getApplication())).get(CriterionViewModel.class);
            for(int i = 0; i < criterionBridgeId.size(); i++){
                mCriterionViewModel.editCriterion(criterionBridgeId.get(i), ((RenameActivity) getActivity()).getTaskIdForPreview(), false);
            }
        }
    }
}
