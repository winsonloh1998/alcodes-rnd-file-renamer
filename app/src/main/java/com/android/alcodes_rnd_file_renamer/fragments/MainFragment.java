package com.android.alcodes_rnd_file_renamer.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsProvider;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.alcodes_rnd_file_renamer.R;
import com.android.alcodes_rnd_file_renamer.adapters.FileAdapter;
import com.android.alcodes_rnd_file_renamer.database.entities.File;
import com.android.alcodes_rnd_file_renamer.utils.SharedPreferenceHelper;
import com.android.alcodes_rnd_file_renamer.utils.UriDeserializerHelper;
import com.android.alcodes_rnd_file_renamer.utils.UriSerializerHelper;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment implements FileAdapter.Callbacks{
    private static final String OUTSTATE_OPENED_FILE_LIST = "OUTSTATE_OPENED_FILE_LIST";

    private static final int OPEN_DOCUMENT_REQUEST_CODE = 41;
    private static final int OPEN_FOLDER_REQUEST_CODE = 42;

    private Animation fabOpen, fabClose, fabClock, fabAntiClock;

    protected ViewPager mViewPagerContent;

    protected TextView mTextViewChooseFile;
    protected TextView mTextViewChooseFolder;
    protected TextView mTextViewNoFileSelected;
    protected FloatingActionButton mFloatingActionButtonAddFile;
    protected FloatingActionButton mFloatingActionButtonAddFolder;
    protected FloatingActionButton mFloatingActionButtonGeneral;
    protected RecyclerView mRecyclerViewFileList;
    protected MenuItem mMenuItemDelete;
    protected MenuItem mMenuItemNext;

    private boolean isOpen = false;
    private List<File> fileList = new ArrayList<>();
    private FileAdapter mAdapter;
    private Callbacks mCallback;

    public MainFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        mMenuItemDelete = menu.findItem(R.id.item_delete_button);
        mMenuItemNext = menu.findItem(R.id.item_next_button);

        uiButtonManagement();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initAnimation();

        if(savedInstanceState != null){
            initSavedStateData(savedInstanceState);
        }
    }

    private void initSavedStateData(Bundle savedInstanceState){
        fileList = new ArrayList<>();
        Type fileListType = new TypeToken<List<File>>(){}.getType();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class ,new UriDeserializerHelper())
                .create();
        fileList = gson.fromJson(savedInstanceState.getString(OUTSTATE_OPENED_FILE_LIST), fileListType);
        mAdapter.setData(fileList);
        mAdapter.notifyDataSetChanged();
        viewVisibilityBasedOnFileIsPicked();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferenceHelper.getInstance(getActivity()).edit().clear().apply();
    }

    private void initView(){
        mTextViewChooseFile = getActivity().findViewById(R.id.textview_choose_file);
        mTextViewChooseFolder = getActivity().findViewById(R.id.textview_choose_folder);
        mTextViewNoFileSelected = getActivity().findViewById(R.id.textview_no_file_selected);
        mFloatingActionButtonGeneral = getActivity().findViewById(R.id.floatingactionbutton_file_general);
        mFloatingActionButtonAddFolder = getActivity().findViewById(R.id.floatingactionbutton_add_folder);
        mFloatingActionButtonAddFile = getActivity().findViewById(R.id.floatingactionbutton_add_file);
        mRecyclerViewFileList = getActivity().findViewById(R.id.recyclerview_selected_file);
        mViewPagerContent = getActivity().findViewById(R.id.viewpager_content);

        mFloatingActionButtonGeneral.setOnClickListener(this::fabGeneralOnClick);
        mFloatingActionButtonAddFile.setOnClickListener(this::fabAddFileOnClick);
        mFloatingActionButtonAddFolder.setOnClickListener(this::fabAddFolderOnClick);

        mViewPagerContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 1){
                    saveFileListToSharedPreference();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mAdapter = new FileAdapter();
        mAdapter.setCallBacks(this);
        mRecyclerViewFileList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerViewFileList.setHasFixedSize(true);
        mRecyclerViewFileList.setAdapter(mAdapter);
    }

    private void initAnimation(){
        fabOpen = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_close);
        fabClock = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_rotate_clock);
        fabAntiClock = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_rotate_anticlock);
    }

    private void fabGeneralOnClick(View view){
        if(isOpen){
            mTextViewChooseFile.setVisibility(View.INVISIBLE);
            mTextViewChooseFolder.setVisibility(View.INVISIBLE);
            mFloatingActionButtonAddFile.setAnimation(fabClose);
            mFloatingActionButtonAddFolder.setAnimation(fabClose);
            mFloatingActionButtonGeneral.setAnimation(fabAntiClock);
            mFloatingActionButtonAddFile.setClickable(false);
            mFloatingActionButtonAddFolder.setClickable(false);
            isOpen = false;
        }else{
            mTextViewChooseFile.setVisibility(View.VISIBLE);
            mTextViewChooseFolder.setVisibility(View.VISIBLE);
            mFloatingActionButtonAddFile.setAnimation(fabOpen);
            mFloatingActionButtonAddFolder.setAnimation(fabOpen);
            mFloatingActionButtonGeneral.setAnimation(fabClock);
            mFloatingActionButtonAddFile.setClickable(true);
            mFloatingActionButtonAddFolder.setClickable(true);
            isOpen = true;
        }
    }

    private void fabAddFolderOnClick(View view){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(intent, OPEN_FOLDER_REQUEST_CODE);

        fabGeneralOnClick(view);
    }

    private void fabAddFileOnClick(View view){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, OPEN_DOCUMENT_REQUEST_CODE);

        fabGeneralOnClick(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = null;

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == OPEN_DOCUMENT_REQUEST_CODE) {
                //To Select Single or Multiple Files From Any Folder
                ClipData clipData = data.getClipData();

                if (clipData == null){
                    if (data != null){
                        uri = data.getData();
                        //Prevent Duplication of Files
                        if(!(isExistedInFileList(uri))){
                            initFileObject(uri);
                        }
                    }
                }else{
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item path = clipData.getItemAt(i);
                        uri = path.getUri();
                        //Prevent Duplication of Files
                        if(!(isExistedInFileList(uri))){
                            initFileObject(uri);
                        }
                    }
                }
            }else if (requestCode == OPEN_FOLDER_REQUEST_CODE){
                // To Select All the Files From A Folder
                Uri rootUri = data.getData();
                DocumentFile[] documentFile = DocumentFile.fromTreeUri(getActivity().getApplicationContext(), rootUri).listFiles();

                for(int i=0; i<documentFile.length; i++){
                    uri = documentFile[i].getUri();
                    if(!(isExistedInFileList(uri))){
                        if(!DocumentFile.fromSingleUri(getActivity().getApplication(),uri).isDirectory()){
                            initFileObject(uri);
                        }
                    }
                }
            }

            mAdapter.setData(fileList);
            mAdapter.notifyDataSetChanged();

            viewVisibilityBasedOnFileIsPicked();
        }
    }

    private void viewVisibilityBasedOnFileIsPicked(){
        if(fileList.size() > 0){
            mRecyclerViewFileList.setVisibility(View.VISIBLE);
            mTextViewNoFileSelected.setVisibility(View.INVISIBLE);
        }else{
            mRecyclerViewFileList.setVisibility(View.INVISIBLE);
            mTextViewNoFileSelected.setVisibility(View.VISIBLE);
        }
    }

    private boolean isExistedInFileList(Uri uri){
        boolean result = false;

        for(int i=0; i < fileList.size(); i++){
            if(DocumentsContract.getDocumentId(fileList.get(i).getFileUri()).equals(DocumentsContract.getDocumentId(uri))){
                result = true;
                break;
            }
        }

        return result;
    }

    private void initFileObject(Uri uri){
        String fileName, fileType, fileSize = "";

        if(uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    // To Get the Full Name of The File
                    fileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));

                    // To Get the Size of The File in bytes
                    fileSize = cursor.getString(cursor.getColumnIndex(OpenableColumns.SIZE));

                    // To Get the MimeType of The File
                    fileType = getFileMimeType(uri);

                    File file = new File();
                    file.setFileUri(uri);
                    file.setFileName(fileName);
                    file.setFileSize(fileSize);
                    file.setFileType(fileType);

                    fileList.add(file);
                }
            } finally {
                cursor.close();
            }
        }
    }

    private String getFileMimeType (Uri uri){
        ContentResolver cR = getActivity().getApplicationContext().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();

        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    @Override
    public void onListItemClicked(File data, View view) {
        ImageView mImageViewFileType = (ImageView) view;

        if(data.isChecked()){
            data.setChecked(false);
            if(data.getFileType().toLowerCase().equals("png") ||
                    data.getFileType().toLowerCase().equals("jpg") ||
                    data.getFileType().toLowerCase().equals("gif") ||
                    data.getFileType().toLowerCase().equals("jpeg")){
                Glide.with(getActivity())
                        .load(data.getFileUri())
                        .into(mImageViewFileType);
            }else{
                Uri drawableUri = Uri.parse("android.resource://com.android.alcodes_rnd_file_renamer/drawable/ic_insert_drive_file_blue_24dp");
                Glide.with(getActivity())
                        .load(drawableUri)
                        .into(mImageViewFileType);
            }
        }else{
            data.setChecked(true);
            Uri drawableUri = Uri.parse("android.resource://com.android.alcodes_rnd_file_renamer/drawable/ic_check_blue_24dp");
            Glide.with(getActivity())
                    .load(drawableUri)
                    .into(mImageViewFileType);
        }

        uiButtonManagement();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        List<File> selectedFileList = new ArrayList<>();

        if(mAdapter.getSelectedFile() != null){

            selectedFileList = mAdapter.getSelectedFile();

            switch (item.getItemId()){
                case R.id.item_delete_button:
                    for(int i = 0;i < selectedFileList.size(); i++){
                        for ( int j = 0 ; j < fileList.size(); j++){
                            if(selectedFileList.get(i).getFileUri().equals(fileList.get(j).getFileUri())){
                                fileList.remove(j);
                            }
                        }
                    }
                    mAdapter.notifyDataSetChanged();

                    if(fileList.size() == 0){
                        mRecyclerViewFileList.setVisibility(View.INVISIBLE);
                        mTextViewNoFileSelected.setVisibility(View.VISIBLE);
                        mFloatingActionButtonGeneral.show();
                    }

                    if(mAdapter.getSelectedFile().size() == 0 || fileList.size() == 0){
                        mMenuItemNext.setVisible(false);
                        mMenuItemDelete.setVisible(false);
                        mFloatingActionButtonGeneral.show();
                    }
                    return true;
                case R.id.item_next_button:
                    //Save the List into Shared Preferences by Using GSON
                    saveFileListToSharedPreference();

                    if(mCallback != null){
                        mCallback.onNextIconClicked();
                    }
                    return true;
            }
        }else{
            Toast.makeText(getActivity(), "No File is Selected For Renaming", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriSerializerHelper())
                .create();
        String fileJson = gson.toJson(fileList);
        outState.putString(OUTSTATE_OPENED_FILE_LIST, fileJson);
    }

    private void uiButtonManagement(){
        //When Selected and Select File is Selected, Set Visible to Menu Item
        if(mAdapter.getSelectedFile().size() == 0){
            mMenuItemNext.setVisible(false);
            mMenuItemDelete.setVisible(false);
            mFloatingActionButtonGeneral.show();
        }else{
            mMenuItemNext.setVisible(true);
            mMenuItemDelete.setVisible(true);
            if(isOpen){
                fabGeneralOnClick(getView());
            }
            mFloatingActionButtonGeneral.hide();
        }
    }

    public void setCallback(Callbacks callback){
        mCallback = callback;
    }

    public interface Callbacks {
        public void onNextIconClicked();
    }

    private void saveFileListToSharedPreference(){
        //Clear the Shares Preferences Before Saving Into
        SharedPreferenceHelper.getInstance(getActivity())
                .edit()
                .clear()
                .apply();

        //Use Gson to Convert fileList into Json String
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriSerializerHelper())
                .create();
        String fileJson = gson.toJson(mAdapter.getSelectedFile());

        //Create and Save it into selectedFiles
        SharedPreferenceHelper.getInstance(getActivity())
                .edit()
                .putString("selectedFiles", fileJson)
                .apply();
    }
}
