package com.android.alcodes_rnd_file_renamer.viewmodels.Task;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.android.alcodes_rnd_file_renamer.adapters.TaskAdapter;
import com.android.alcodes_rnd_file_renamer.repositories.TaskRepository;

import java.util.List;

public class TaskViewModel extends AndroidViewModel {

    private TaskRepository mTaskRepository;

    public TaskViewModel(@NonNull Application application) {
        super(application);

        mTaskRepository = TaskRepository.getInstance();
    }

    public LiveData<List<TaskAdapter.DataHolder>> getTaskAdapterListLiveData(){
        return mTaskRepository.getTaskAdapterListLiveData();
    }

    public void addTask(String taskName){
        mTaskRepository.addTask(getApplication(),taskName);
    }

    public void loadTaskAdapterList(){
        mTaskRepository.loadTaskAdapterList(getApplication());
    }

    public void editTask(Long id, String taskName){
        mTaskRepository.editTask(getApplication(), id, taskName);
    }

    public void editIsChecked(Long id, boolean isChecked){
        mTaskRepository.editIsChecked(getApplication(), id, isChecked);
    }

    public void deleteTask(Long id){
        mTaskRepository.deleteTask(getApplication(), id);
    }
}
