package com.android.alcodes_rnd_file_renamer.viewmodels.Criterion;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.android.alcodes_rnd_file_renamer.adapters.CriterionAdapter;
import com.android.alcodes_rnd_file_renamer.repositories.CriterionRepository;

import java.util.List;

public class CriterionViewModel extends AndroidViewModel {

    private CriterionRepository mCriterionRepository;

    public CriterionViewModel(@NonNull Application application){
        super(application);

        mCriterionRepository = CriterionRepository.getInstance();
    }

    public LiveData<List<CriterionAdapter.DataHolder>> getCriterionAdapterListLiveData(){
        return mCriterionRepository.getCriterionAdapterListLiveData();
    }

    public void loadCriterionAdapterList(Long taskId){
        mCriterionRepository.loadCriterionAdapterList(getApplication(), taskId);
    }

    public void addReplaceCharacter(String charToReplace,
                                       String replaceWith, boolean matchCases, Long taskId){
        mCriterionRepository.addReplaceCharacter(getApplication(), charToReplace, replaceWith, matchCases ,taskId);
    }

    public void addNewName(String replaceWithNewName, Long taskId){
        mCriterionRepository.addNewName(getApplication(), replaceWithNewName, taskId);
    }

    public void addInsertCharacters(String charToInsert,
                                    int insertAtOption, int position, boolean backward, Long taskId){
        mCriterionRepository.addInsertCharacters(getApplication(), charToInsert, insertAtOption, position, backward, taskId);
    }

    public void addInsertNumber(String numberToInsert, boolean autoNumber,
                                int incrementDecrementOption, int incrementDecrementValue,
                                int insertAtOption, int position, boolean backward, Long taskId){
        mCriterionRepository.addInsertNumber(getApplication(), numberToInsert, autoNumber,
                incrementDecrementOption, incrementDecrementValue, insertAtOption, position, backward, taskId);
    }

    public void addInsertFileSize(int formatSizeOption, int decimalPlaces,
                                  boolean showUnit, int insertAtOption, int position, boolean backward, Long taskId){
        mCriterionRepository.addInsertFileSize(getApplication(), formatSizeOption, decimalPlaces,
                showUnit, insertAtOption, position, backward, taskId);
    }

    public void deleteReplaceCharacter(Long id, String typeOfCriterion, Long taskId){
        mCriterionRepository.deleteReplaceCharacter(getApplication(), id, typeOfCriterion, taskId);
    }

    public void deleteNewName(Long id, String typeOfCriterion, Long taskId){
        mCriterionRepository.deleteNewName(getApplication(), id, typeOfCriterion, taskId);
    }

    public void deleteInsertCharacters(Long id, String typeOfCriterion, Long taskId){
        mCriterionRepository.deleteInsertCharacters(getApplication(), id, typeOfCriterion, taskId);
    }

    public void deleteInsertNumber(Long id, String typeOfCriterion, Long taskId){
        mCriterionRepository.deleteInsertNumber(getApplication(), id, typeOfCriterion, taskId);
    }

    public void deleteInsertFileSize(Long id, String typeOfCriterion, Long taskId){
        mCriterionRepository.deleteInsertFileSize(getApplication(), id, typeOfCriterion, taskId);
    }

    public void editCriterion(Long id, Long taskId, boolean isChecked){
        mCriterionRepository.editCriterion(getApplication(), id, taskId, isChecked);
    }

    public void editReplaceCharacter(Long id, String charToReplace,
                                     String replaceWith, boolean matchCases, Long taskId){
        mCriterionRepository.editReplaceCharacter(getApplication(), id, charToReplace, replaceWith, matchCases, taskId);
    }

    public void editNewName(Long id, String replaceWithNewName, Long taskId){
        mCriterionRepository.editNewName(getApplication(), id, replaceWithNewName, taskId);
    }

    public void editInsertCharacters(Long id, String charToInsert,
                                     int insertAtOption, int position, boolean backward, Long taskId){
        mCriterionRepository.editInsertCharacters(getApplication(), id, charToInsert, insertAtOption, position, backward, taskId);
    }

    public void editInsertNumber(Long id, String numberToInsert, boolean autoNumber,
                                 int incrementDecrementOption, int incrementDecrementValue, int insertAtOption,
                                 int position, boolean backward, Long taskId){
        mCriterionRepository.editInsertNumber(getApplication(), id, numberToInsert, autoNumber,
                incrementDecrementOption, incrementDecrementValue, insertAtOption, position, backward, taskId);
    }

    public void editInsertFileSize(Long id, int formatSizeOption, int decimalPlaces,
                                   boolean showUnit, int insertAtOption, int position, boolean backward, Long taskId){
        mCriterionRepository.editInsertFileSize(getApplication(), id, formatSizeOption, decimalPlaces, showUnit,
                insertAtOption, position, backward, taskId);
    }
}
