package com.android.alcodes_rnd_file_renamer.viewmodels.Criterion;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class CriterionViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public CriterionViewModelFactory(Application application){mApplication = application;}
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CriterionViewModel(mApplication);
    }
}
