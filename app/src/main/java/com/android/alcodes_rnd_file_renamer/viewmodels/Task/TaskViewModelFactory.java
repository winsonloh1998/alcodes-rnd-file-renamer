package com.android.alcodes_rnd_file_renamer.viewmodels.Task;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class TaskViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public TaskViewModelFactory(Application application) { mApplication = application; }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TaskViewModel(mApplication);
    }
}
