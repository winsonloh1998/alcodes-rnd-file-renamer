package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

import java.util.Date;

@Entity
public class Task {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private String taskName;

    @NonNull
    private Date createdDate;

    private boolean isChecked;

    @Generated(hash = 1610875441)
    public Task(Long id, @NonNull String taskName, @NonNull Date createdDate,
            boolean isChecked) {
        this.id = id;
        this.taskName = taskName;
        this.createdDate = createdDate;
        this.isChecked = isChecked;
    }

    @Generated(hash = 733837707)
    public Task() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean getIsChecked() {
        return this.isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
    
}
