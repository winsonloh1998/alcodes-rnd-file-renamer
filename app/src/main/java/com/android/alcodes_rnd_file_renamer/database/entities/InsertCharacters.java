package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertCharacters {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private String charactersToInsert;

    @NonNull
    private int insertAtOption;

    @Nullable
    private int position;

    @Nullable
    private boolean backward;

    @Generated(hash = 449988063)
    public InsertCharacters(Long id, @NonNull String charactersToInsert,
            int insertAtOption, int position, boolean backward) {
        this.id = id;
        this.charactersToInsert = charactersToInsert;
        this.insertAtOption = insertAtOption;
        this.position = position;
        this.backward = backward;
    }

    @Generated(hash = 1554334917)
    public InsertCharacters() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCharactersToInsert() {
        return this.charactersToInsert;
    }

    public void setCharactersToInsert(String charactersToInsert) {
        this.charactersToInsert = charactersToInsert;
    }

    public int getInsertAtOption() {
        return this.insertAtOption;
    }

    public void setInsertAtOption(int insertAtOption) {
        this.insertAtOption = insertAtOption;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean getBackward() {
        return this.backward;
    }

    public void setBackward(boolean backward) {
        this.backward = backward;
    }
}
