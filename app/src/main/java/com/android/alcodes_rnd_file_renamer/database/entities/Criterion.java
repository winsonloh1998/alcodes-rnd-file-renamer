package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import java.io.Serializable;
import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Criterion{
    @Id(autoincrement = true)
    private Long Id;

    @NonNull
    private Long taskId;

    @NonNull
    private Long criterionId;

    @NonNull
    private Date createdDate;

    @NonNull
    private String typeOfCriterion;

    private boolean isChecked;

    @Generated(hash = 985487417)
    public Criterion(Long Id, @NonNull Long taskId, @NonNull Long criterionId,
            @NonNull Date createdDate, @NonNull String typeOfCriterion,
            boolean isChecked) {
        this.Id = Id;
        this.taskId = taskId;
        this.criterionId = criterionId;
        this.createdDate = createdDate;
        this.typeOfCriterion = typeOfCriterion;
        this.isChecked = isChecked;
    }

    @Generated(hash = 1709435907)
    public Criterion() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getCriterionId() {
        return this.criterionId;
    }

    public void setCriterionId(Long criterionId) {
        this.criterionId = criterionId;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTypeOfCriterion() {
        return this.typeOfCriterion;
    }

    public void setTypeOfCriterion(String typeOfCriterion) {
        this.typeOfCriterion = typeOfCriterion;
    }

    public boolean getIsChecked() {
        return this.isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
    
}
