package com.android.alcodes_rnd_file_renamer.database.entities;

import android.net.Uri;

import java.io.Serializable;

public class File implements Serializable {
    private Uri fileUri;
    private String fileName;
    private String fileSize;
    private String fileType;

    private boolean isChecked = false;

    public File() {
    }

    public File(Uri fileUri, String fileName, String fileSize, String fileType) {
        this.fileUri = fileUri;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileType = fileType;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public boolean isChecked(){
        return isChecked;
    }

    public void setFileUri(Uri fileUri) {
        this.fileUri = fileUri;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
