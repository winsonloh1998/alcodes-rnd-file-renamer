package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertFileSize {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private int formatSizeOption;

    @NonNull
    private int decimalPlaces;

    @NonNull
    private boolean showUnit;

    @NonNull
    private int insertAtOption;

    @Nullable
    private int position;

    @Nullable
    private boolean backward;

    @Generated(hash = 1402789720)
    public InsertFileSize(Long id, int formatSizeOption, int decimalPlaces,
            boolean showUnit, int insertAtOption, int position, boolean backward) {
        this.id = id;
        this.formatSizeOption = formatSizeOption;
        this.decimalPlaces = decimalPlaces;
        this.showUnit = showUnit;
        this.insertAtOption = insertAtOption;
        this.position = position;
        this.backward = backward;
    }

    @Generated(hash = 1846507418)
    public InsertFileSize() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFormatSizeOption() {
        return this.formatSizeOption;
    }

    public void setFormatSizeOption(int formatSizeOption) {
        this.formatSizeOption = formatSizeOption;
    }

    public int getDecimalPlaces() {
        return this.decimalPlaces;
    }

    public void setDecimalPlaces(int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    public boolean getShowUnit() {
        return this.showUnit;
    }

    public void setShowUnit(boolean showUnit) {
        this.showUnit = showUnit;
    }

    public int getInsertAtOption() {
        return this.insertAtOption;
    }

    public void setInsertAtOption(int insertAtOption) {
        this.insertAtOption = insertAtOption;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean getBackward() {
        return this.backward;
    }

    public void setBackward(boolean backward) {
        this.backward = backward;
    }
}
