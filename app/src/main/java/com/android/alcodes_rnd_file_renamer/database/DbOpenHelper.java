package com.android.alcodes_rnd_file_renamer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.android.alcodes_rnd_file_renamer.database.entities.DaoMaster;

public class DbOpenHelper extends DaoMaster.OpenHelper {
    public DbOpenHelper(Context context, String name) { super(context,name); }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }
}
