package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class NewName {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private String newName;

    @Generated(hash = 449517960)
    public NewName(Long id, @NonNull String newName) {
        this.id = id;
        this.newName = newName;
    }

    @Generated(hash = 991206095)
    public NewName() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewName() {
        return this.newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
