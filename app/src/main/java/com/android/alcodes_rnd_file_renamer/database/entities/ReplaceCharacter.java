package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;


@Entity
public class ReplaceCharacter {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private String charactersToReplace;

    @NonNull
    private String replaceWith;

    @NonNull
    private boolean matchCases;

    @Generated(hash = 19472107)
    public ReplaceCharacter(Long id, @NonNull String charactersToReplace,
            @NonNull String replaceWith, boolean matchCases) {
        this.id = id;
        this.charactersToReplace = charactersToReplace;
        this.replaceWith = replaceWith;
        this.matchCases = matchCases;
    }

    @Generated(hash = 1945331186)
    public ReplaceCharacter() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCharactersToReplace() {
        return this.charactersToReplace;
    }

    public void setCharactersToReplace(String charactersToReplace) {
        this.charactersToReplace = charactersToReplace;
    }

    public String getReplaceWith() {
        return this.replaceWith;
    }

    public void setReplaceWith(String replaceWith) {
        this.replaceWith = replaceWith;
    }

    public boolean getMatchCases() {
        return this.matchCases;
    }

    public void setMatchCases(boolean matchCases) {
        this.matchCases = matchCases;
    }
}
