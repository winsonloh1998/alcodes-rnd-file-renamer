package com.android.alcodes_rnd_file_renamer.database.entities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertNumber {
    @Id(autoincrement = true)
    private Long id;

    @NonNull
    private String numbersToInsert;

    @Nullable
    private boolean autoNumber;

    @Nullable
    private int incrementDecrementOption;

    @Nullable
    private int incrementDecrementValue;

    @NonNull
    private int insertAtOption;

    @Nullable
    private int position;

    @Nullable
    private boolean backward;

    @Generated(hash = 1140136112)
    public InsertNumber(Long id, @NonNull String numbersToInsert,
            boolean autoNumber, int incrementDecrementOption,
            int incrementDecrementValue, int insertAtOption, int position,
            boolean backward) {
        this.id = id;
        this.numbersToInsert = numbersToInsert;
        this.autoNumber = autoNumber;
        this.incrementDecrementOption = incrementDecrementOption;
        this.incrementDecrementValue = incrementDecrementValue;
        this.insertAtOption = insertAtOption;
        this.position = position;
        this.backward = backward;
    }

    @Generated(hash = 1787458044)
    public InsertNumber() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumbersToInsert() {
        return this.numbersToInsert;
    }

    public void setNumbersToInsert(String numbersToInsert) {
        this.numbersToInsert = numbersToInsert;
    }

    public boolean getAutoNumber() {
        return this.autoNumber;
    }

    public void setAutoNumber(boolean autoNumber) {
        this.autoNumber = autoNumber;
    }

    public int getIncrementDecrementOption() {
        return this.incrementDecrementOption;
    }

    public void setIncrementDecrementOption(int incrementDecrementOption) {
        this.incrementDecrementOption = incrementDecrementOption;
    }

    public int getIncrementDecrementValue() {
        return this.incrementDecrementValue;
    }

    public void setIncrementDecrementValue(int incrementDecrementValue) {
        this.incrementDecrementValue = incrementDecrementValue;
    }

    public int getInsertAtOption() {
        return this.insertAtOption;
    }

    public void setInsertAtOption(int insertAtOption) {
        this.insertAtOption = insertAtOption;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean getBackward() {
        return this.backward;
    }

    public void setBackward(boolean backward) {
        this.backward = backward;
    }

}
